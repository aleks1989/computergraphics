//
//  Memory.h
//  Tinyrenderer
//
//  Created by Aleksandr Borodulin on 14/03/2019.
//  Copyright © 2019 Aleksandr Borodulin. All rights reserved.
//

#ifndef Memory_h
#define Memory_h

inline void Mem_Set_Int(void * dest, int data, size_t count) {
#if defined(__x86_64__) || defined(_M_X64)
    asm
    {
        mov eax, count
        test eax, 1
        jnz Odd
        jz Even

    Odd:
        mov rdi, dest
        mov ecx, count
        mov eax, data
        rep stosd
        jmp Exit

    Even:
        mov rdx, data
        shrd rax, rdx, 32
        shrd rax, rdx, 32
        mov rdi, dest
        mov rcx, count
        shr rcx, 1
        rep stosq
    Exit:
    }
#else
    int* lastElement = (int*)dest + count - 1;
    for (int* d = (int*)dest; d <= lastElement; d++) {
        *d = data;
    }
#endif
}

#endif /* Memory_h */
