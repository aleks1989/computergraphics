#ifndef __GEOMETRY_H__
#define __GEOMETRY_H__

#include <cmath>
#include <iostream>
#include <cassert>
#include "Array.h"

#define EPS_VALUE 0.00001

template <class t> struct Vec2 {
    union {
        struct {t x, y;};
        t raw[2];
    };
    
    Vec2<t>() : x(t()), y(t()) {}
    Vec2<t>(t _x, t _y) : x(_x), y(_y) {}
    Vec2<t>(const Vec2<t> &v) : x(t()), y(t()) { *this = v; }
    
    template <class u>
    Vec2<t> & operator =(const Vec2<u> &v) {
        if (this != (Vec2<t>*)&v) {
            x = v.x;
            y = v.y;
        }
        return *this;
    }
    Vec2<t> operator +(const Vec2<t> &V) const { return Vec2<t>(x+V.x, y+V.y); }
    Vec2<t> operator -(const Vec2<t> &V) const { return Vec2<t>(x-V.x, y-V.y); }
    Vec2<t> operator *(float f)          const { return Vec2<t>(x*f, y*f); }
    Vec2<t> operator /(float f)          const { return Vec2<t>(x/f, y/f); }
    t& operator[](const int i) { if (x<=0) return x; else return y; }
    template <class > friend std::ostream& operator<<(std::ostream& s, Vec2<t>& v);
};

template <class t> struct Vec3 {
    union {
        struct {t x, y, z;};
        t raw[3];
    };
    Vec3<t>() : x(t()), y(t()), z(t()) { }
    Vec3<t>(t _x, t _y, t _z) : x(_x), y(_y), z(_z) {}
    template <class u> Vec3<t>(const Vec3<u> &v);
    Vec3<t>(const Vec3<t> &v) : x(t()), y(t()), z(t()) { *this = v; }
    Vec3<t> & operator =(const Vec3<t> &v) {
        if (this != &v) {
            x = v.x;
            y = v.y;
            z = v.z;
        }
        return *this;
    }
    template <class u>
    Vec3<t> & operator =(const Vec3<u> &v) {
        if (this != (Vec3<t>*)&v) {
            x = v.x;
            y = v.y;
            z = v.z;
        }
        return *this;
    }
    Vec3<t> operator ^(const Vec3<t> &v) const { return Vec3<t>(y*v.z-z*v.y, z*v.x-x*v.z, x*v.y-y*v.x); }
    Vec3<t> operator +(const Vec3<t> &v) const { return Vec3<t>(x+v.x, y+v.y, z+v.z); }
    Vec3<t> operator -(const Vec3<t> &v) const { return Vec3<t>(x-v.x, y-v.y, z-v.z); }
    Vec3<t> operator *(float f)          const { return Vec3<t>(x*f, y*f, z*f); }
    t       operator *(const Vec3<t> &v) const { return x*v.x + y*v.y + z*v.z; }
    float norm () const { return std::sqrt(x*x+y*y+z*z); }
    Vec3<t> & normalize() { *this = (*this)*(1/norm()); return *this; }
    t& operator[](const int i) { if (i<=0) return x; else if (i==1) return y; else return z; }
    template <class > friend std::ostream& operator<<(std::ostream& s, Vec3<t>& v);
};

template <class t> struct Vec4 {
    union {
        struct {t x, y, z, w;};
        t raw[4];
    };
    Vec4<t>() : x(t()), y(t()), z(t()), w(t()) { }
    Vec4<t>(t _x, t _y, t _z, t _w) : x(_x), y(_y), z(_z), w(_w) {}
    template <class u> Vec4<t>(const Vec4<u> &v) : x(v.x), y(v.y), z(v.z), w(v.w) {}
    Vec4<t>(const Vec4<t> &v) : x(t()), y(t()), z(t()), w(t()) { *this = v; }
    Vec4<t>(const Vec3<t> &v) : x(v.x), y(v.y), z(v.z), w(1) {}
    Vec4<t> & operator =(const Vec4<t> &v) {
        if (this != &v) {
            x = v.x;
            y = v.y;
            z = v.z;
            w = v.w;
        }
        return *this;
    }
    template <class u>
    Vec4<t> & operator =(const Vec4<u> &v) {
        if (this != (Vec4<t>*)&v) {
            x = v.x;
            y = v.y;
            z = v.z;
            w = v.w;
        }
        return *this;
    }
    
    Vec4<t> & operator =(const Vec3<t> &v) {
        x = v.x;
        y = v.y;
        z = v.z;
        w = 1;
        return *this;
    }
    
    template <class u>
    Vec4<t> & operator =(const Vec3<u> &v) {
        x = v.x;
        y = v.y;
        z = v.z;
        w = 1;
        return *this;
    }
    Vec3<t> operator ^(const Vec4<t> &v) const
    {
        return Vec3<t>(y*v.z-z*v.y, z*v.x-x*v.z, x*v.y-y*v.x);
    }
    Vec4<t> operator +(const Vec4<t> &v) const { return Vec4<t>(x+v.x, y+v.y, z+v.z, w + v.w); }
    Vec4<t> operator -(const Vec4<t> &v) const { return Vec4<t>(x-v.x, y-v.y, z-v.z, w - v.w); }
    Vec4<t> operator *(float f)          const { return Vec4<t>(x*f, y*f, z*f, w*f); }
    t       operator *(const Vec3<t> &v) const { return x*v.x + y*v.y + z*v.z; }
    t       operator *(const Vec4<t> &v) const { return x*v.x + y*v.y + z*v.z; }
    float norm() const { return std::sqrt(x*x+y*y+z*z); }
    Vec4<t> & normalize() { *this = (*this)*(1/norm()); return *this; }
    t& operator[](const int i) { if (i<=0) return x; else if (i==1) return y; else if (i==2) return z; else return w; }
    template <class > friend std::ostream& operator<<(std::ostream& s, Vec4<t>& v);
};

typedef Vec2<float> Vec2f;
typedef Vec2<double> Vec2df;
typedef Vec2<int>   Vec2i;
typedef Vec3<float> Vec3f;
typedef Vec3<double> Vec3df;
typedef Vec3<int>   Vec3i;
typedef Vec4<float> Vec4f;
typedef Vec4<int>   Vec4i;
typedef Vec4<double> Vec4df;

template <> template <> Vec3<int>::Vec3(const Vec3<float> &v);
template <> template <> Vec3<float>::Vec3(const Vec3<int> &v);

template <class t> t Dot(Vec3<t> &p0, Vec3<t> &p1, Vec3<t> &p2) {
    t E = (p2.x - p0.x) * (p1.y - p0.y) - (p2.y - p0.y) * (p1.x - p0.x);
    return E;
}

template <class t> t Dot(Vec2<t> &p0, Vec2<t> &p1, Vec2<t> &p2) {
    t E = (p2.x - p0.x) * (p1.y - p0.y) - (p2.y - p0.y) * (p1.x - p0.x);
    return E;
}

template <class t> inline t Dot2(Vec2<t> &p0, Vec2<t> &p1, Vec2<t> &p2) {
    t E = (p2.x - p0.x) * (p1.y - p0.y) - (p2.y - p0.y) * (p1.x - p0.x);
    return E;
}

template <class t> std::ostream& operator<<(std::ostream& s, Vec2<t>& v) {
    s << "(" << v.x << ", " << v.y << ")\n";
    return s;
}

template <class t> std::ostream& operator<<(std::ostream& s, Vec3<t>& v) {
    s << "(" << v.x << ", " << v.y << ", " << v.z << ")\n";
    return s;
}

template <class t> std::ostream& operator<<(std::ostream& s, Vec4<t>& v) {
    s << "(" << v.x << ", " << v.y << ", " << v.z << ", " << v.w << ")\n";
    return s;
}

//////////////////////////////////////////////////////////////////////////////////////////////

template <unsigned long N, unsigned long M>
class Matrix;

typedef Matrix<3, 3> Mat3_3;
typedef Matrix<4, 4> Mat4_4;

template <unsigned long N, unsigned long M>
class Matrix {
public:
    union {
        float raw[N*M];
        float m[N][M];
    };

    Matrix<N, M>();
    
    static Matrix<N, M> identity();
    float* operator[](const int i);
    Matrix<N, M> operator*(const Matrix<N, M>& a);
    inline Vec4f operator*(const Vec4f& v);
    inline Vec4f operator*(const Vec3f& v);
    Matrix<N, M> transpose() const;
    static Matrix<N, M> rotateX(float angle);
    static Mat4_4 rotateZ(float angle);
    static Mat4_4 translate(float x, float y, float z);
    static Mat4_4 scale(float x, float y, float z);
    
    friend std::ostream& operator<<(std::ostream& s, Mat4_4& m);
    
    Mat4_4 invert();
private:
    float minor(float m[16], int r0, int r1, int r2, int c0, int c1, int c2);
    void adjoint(float m[16], float adjOut[16]);
    float det(float m[16]);
    float determinant(int n);
};

//template <unsigned long N, unsigned long M>
std::ostream& operator<<(std::ostream& s, Mat4_4& m);

template <unsigned long N, unsigned long M> Vec4f Matrix<N, M>::operator*(const Vec4f& v) {
    if (N == 4 && M == 4) {
        Vec4f result;
        result.raw[0] = m[0][0]*v.raw[0] + m[0][1]*v.raw[1] + m[0][2]*v.raw[2] + m[0][3]*v.raw[3];
        result.raw[1] = m[1][0]*v.raw[0] + m[1][1]*v.raw[1] + m[1][2]*v.raw[2] + m[1][3]*v.raw[3];
        result.raw[2] = m[2][0]*v.raw[0] + m[2][1]*v.raw[1] + m[2][2]*v.raw[2] + m[2][3]*v.raw[3];
        result.raw[3] = m[3][0]*v.raw[0] + m[3][1]*v.raw[1] + m[3][2]*v.raw[2] + m[3][3]*v.raw[3];
        return result;
    } else if (N == 3 && M == 3) {
        Vec4f result;
        result.raw[0] = m[0][0]*v.raw[0] + m[0][1]*v.raw[1] + m[0][2]*v.raw[2];
        result.raw[1] = m[1][0]*v.raw[0] + m[1][1]*v.raw[1] + m[1][2]*v.raw[2];
        result.raw[2] = m[2][0]*v.raw[0] + m[2][1]*v.raw[1] + m[2][2]*v.raw[2];
        result.raw[3] = 1.f;
        return result;
    }
    assert(false);
    return Vec4f();
}

template <unsigned long N, unsigned long M> Vec4f Matrix<N, M>::operator*(const Vec3f& v) {
    if (N == 4 && M == 4) {
        Vec4f result;
        result.raw[0] = m[0][0]*v.raw[0] + m[0][1]*v.raw[1] + m[0][2]*v.raw[2] + m[0][3];
        result.raw[1] = m[1][0]*v.raw[0] + m[1][1]*v.raw[1] + m[1][2]*v.raw[2] + m[1][3];
        result.raw[2] = m[2][0]*v.raw[0] + m[2][1]*v.raw[1] + m[2][2]*v.raw[2] + m[2][3];
        result.raw[3] = m[3][0]*v.raw[0] + m[3][1]*v.raw[1] + m[3][2]*v.raw[2] + m[3][3];
        return result;
    } else if (N == 3 && M == 3) {
        Vec4f result;
        result.raw[0] = m[0][0]*v.raw[0] + m[0][1]*v.raw[1] + m[0][2]*v.raw[2];
        result.raw[1] = m[1][0]*v.raw[0] + m[1][1]*v.raw[1] + m[1][2]*v.raw[2];
        result.raw[2] = m[2][0]*v.raw[0] + m[2][1]*v.raw[1] + m[2][2]*v.raw[2];
        result.raw[3] = 1.f;
        return result;
    }
}

template <unsigned long N, unsigned long M> Matrix<N, M>::Matrix() { }

template <unsigned long N, unsigned long M> Matrix<N, M> Matrix<N, M>::identity() {
    Matrix id;
    id[0][0] = 1.f; id[0][1] = 0.f; id[0][2] = 0.f; id[0][3] = 0.f;
    id[1][0] = 0.f; id[1][1] = 1.f; id[1][2] = 0.f; id[1][3] = 0.f;
    id[2][0] = 0.f; id[2][1] = 0.f; id[2][2] = 1.f; id[2][3] = 0.f;
    id[3][0] = 0.f; id[3][1] = 0.f; id[3][2] = 0.f; id[3][3] = 1.f;
    return id;
}

template <unsigned long N, unsigned long M> float* Matrix<N, M>::operator[](const int i) {
    return m[i];
}

template <unsigned long N, unsigned long M> Matrix<N, M> Matrix<N, M>::operator*(const Matrix<N, M>& a) {
    Matrix result;

    result.m[0][0] = m[0][0]*a.m[0][0] + m[0][1]*a.m[1][0] + m[0][2]*a.m[2][0] + m[0][3]*a.m[3][0];
    result.m[0][1] = m[0][0]*a.m[0][1] + m[0][1]*a.m[1][1] + m[0][2]*a.m[2][1] + m[0][3]*a.m[3][1];
    result.m[0][2] = m[0][0]*a.m[0][2] + m[0][1]*a.m[1][2] + m[0][2]*a.m[2][2] + m[0][3]*a.m[3][2];
    result.m[0][3] = m[0][0]*a.m[0][3] + m[0][1]*a.m[1][3] + m[0][2]*a.m[2][3] + m[0][3]*a.m[3][3];

    result.m[1][0] = m[1][0]*a.m[0][0] + m[1][1]*a.m[1][0] + m[1][2]*a.m[2][0] + m[1][3]*a.m[3][0];
    result.m[1][1] = m[1][0]*a.m[0][1] + m[1][1]*a.m[1][1] + m[1][2]*a.m[2][1] + m[1][3]*a.m[3][1];
    result.m[1][2] = m[1][0]*a.m[0][2] + m[1][1]*a.m[1][2] + m[1][2]*a.m[2][2] + m[1][3]*a.m[3][2];
    result.m[1][3] = m[1][0]*a.m[0][3] + m[1][1]*a.m[1][3] + m[1][2]*a.m[2][3] + m[1][3]*a.m[3][3];

    result.m[2][0] = m[2][0]*a.m[0][0] + m[2][1]*a.m[1][0] + m[2][2]*a.m[2][0] + m[2][3]*a.m[3][0];
    result.m[2][1] = m[2][0]*a.m[0][1] + m[2][1]*a.m[1][1] + m[2][2]*a.m[2][1] + m[2][3]*a.m[3][1];
    result.m[2][2] = m[2][0]*a.m[0][2] + m[2][1]*a.m[1][2] + m[2][2]*a.m[2][2] + m[2][3]*a.m[3][2];
    result.m[2][3] = m[2][0]*a.m[0][3] + m[2][1]*a.m[1][3] + m[2][2]*a.m[2][3] + m[2][3]*a.m[3][3];

    result.m[3][0] = m[3][0]*a.m[0][0] + m[3][1]*a.m[1][0] + m[3][2]*a.m[2][0] + m[3][3]*a.m[3][0];
    result.m[3][1] = m[3][0]*a.m[0][1] + m[3][1]*a.m[1][1] + m[3][2]*a.m[2][1] + m[3][3]*a.m[3][1];
    result.m[3][2] = m[3][0]*a.m[0][2] + m[3][1]*a.m[1][2] + m[3][2]*a.m[2][2] + m[3][3]*a.m[3][2];
    result.m[3][3] = m[3][0]*a.m[0][3] + m[3][1]*a.m[1][3] + m[3][2]*a.m[2][3] + m[3][3]*a.m[3][3];

    return result;
}

template <unsigned long N, unsigned long M> Matrix<N, M> Matrix<N, M>::transpose() const {
    Matrix result;
    for(int i=0; i<N; i++)
        for(int j=0; j<M; j++)
            result[j][i] = m[i][j];
    return result;
}

template <unsigned long N, unsigned long M> Matrix<N, M> Matrix<N, M>::rotateX(float angle) {
    if (N == 4 && M == 4) {
        Matrix<N, M> rMat;
        rMat[0][0] = 1.f; rMat[0][1] = 0.f;        rMat[0][2] = 0.f;         rMat[0][3] = 0.f;
        rMat[1][0] = 0.f; rMat[1][1] = cos(angle); rMat[1][2] = -sin(angle); rMat[1][3] = 0.f;
        rMat[2][0] = 0.f; rMat[2][1] = sin(angle); rMat[2][2] = cos(angle);  rMat[2][3] = 0.f;
        rMat[3][0] = 0.f; rMat[3][1] = 0.f;        rMat[3][2] = 0.f;         rMat[3][3] = 1.f;
        return rMat;
    } else if (N == 3 && M == 3) {
        Matrix<N, M> rMat;
        rMat[0][0] = 1.f; rMat[0][1] = 0.f;        rMat[0][2] = 0.f;
        rMat[1][0] = 0.f; rMat[1][1] = cos(angle); rMat[1][2] = -sin(angle);
        rMat[2][0] = 0.f; rMat[2][1] = sin(angle); rMat[2][2] = cos(angle);
        return rMat;
    }
    assert(false);
    return Matrix<N, M>();
}

template <unsigned long N, unsigned long M> Mat4_4 Matrix<N, M>::rotateZ(float angle) {
    Matrix rMat;
    rMat[0][0] = cos(angle); rMat[0][1] = -sin(angle); rMat[0][2] = 0.f; rMat[0][3] = 0.f;
    rMat[1][0] = sin(angle); rMat[1][1] = cos(angle);  rMat[1][2] = 0.f; rMat[1][3] = 0.f;
    rMat[2][0] = 0.f;        rMat[2][1] = 0.f;         rMat[2][2] = 1.f; rMat[2][3] = 0.f;
    rMat[3][0] = 0.f;        rMat[3][1] = 0.f;         rMat[3][2] = 0.f; rMat[3][3] = 1.f;
    return rMat;
}

template <unsigned long N, unsigned long M> Mat4_4 Matrix<N, M>::translate(float x, float y, float z) {
    Matrix tMat;
    tMat[0][0] = 1.f; tMat[0][1] = 0.f; tMat[0][2] = 0.f; tMat[0][3] = x;
    tMat[1][0] = 0.f; tMat[1][1] = 1.f; tMat[1][2] = 0.f; tMat[1][3] = y;
    tMat[2][0] = 0.f; tMat[2][1] = 0.f; tMat[2][2] = 1.f; tMat[2][3] = z;
    tMat[3][0] = 0.f; tMat[3][1] = 0.f; tMat[3][2] = 0.f; tMat[3][3] = 1.f;
    return tMat;
}

template <unsigned long N, unsigned long M> Mat4_4 Matrix<N, M>::scale(float x, float y, float z) {
    Matrix mat;
    mat[0][0] = x;   mat[0][1] = 0.f; mat[0][2] = 0.f; mat[0][3] = 0.f;
    mat[1][0] = 0.f; mat[1][1] = y;   mat[1][2] = 0.f; mat[1][3] = 0.f;
    mat[2][0] = 0.f; mat[2][1] = 0.f; mat[2][2] = z;   mat[2][3] = 0.f;
    mat[3][0] = 0.f; mat[3][1] = 0.f; mat[3][2] = 0.f; mat[3][3] = 1.f;
    return mat;
}

template <unsigned long N, unsigned long M> float Matrix<N, M>::minor(float m[16], int r0, int r1, int r2, int c0, int c1, int c2)
{
    return m[4*r0+c0] * (m[4*r1+c1] * m[4*r2+c2] - m[4*r2+c1] * m[4*r1+c2]) -
    m[4*r0+c1] * (m[4*r1+c0] * m[4*r2+c2] - m[4*r2+c0] * m[4*r1+c2]) +
    m[4*r0+c2] * (m[4*r1+c0] * m[4*r2+c1] - m[4*r2+c0] * m[4*r1+c1]);
}


template <unsigned long N, unsigned long M> void Matrix<N, M>::adjoint(float m[16], float adjOut[16])
{
    adjOut[ 0] =  minor(m,1,2,3,1,2,3); adjOut[ 1] = -minor(m,0,2,3,1,2,3); adjOut[ 2] =  minor(m,0,1,3,1,2,3); adjOut[ 3] = -minor(m,0,1,2,1,2,3);
    adjOut[ 4] = -minor(m,1,2,3,0,2,3); adjOut[ 5] =  minor(m,0,2,3,0,2,3); adjOut[ 6] = -minor(m,0,1,3,0,2,3); adjOut[ 7] =  minor(m,0,1,2,0,2,3);
    adjOut[ 8] =  minor(m,1,2,3,0,1,3); adjOut[ 9] = -minor(m,0,2,3,0,1,3); adjOut[10] =  minor(m,0,1,3,0,1,3); adjOut[11] = -minor(m,0,1,2,0,1,3);
    adjOut[12] = -minor(m,1,2,3,0,1,2); adjOut[13] =  minor(m,0,2,3,0,1,2); adjOut[14] = -minor(m,0,1,3,0,1,2); adjOut[15] =  minor(m,0,1,2,0,1,2);
}

template <unsigned long N, unsigned long M> float Matrix<N, M>::det(float m[16])
{
    return m[0] * minor(m, 1, 2, 3, 1, 2, 3) -
    m[1] * minor(m, 1, 2, 3, 0, 2, 3) +
    m[2] * minor(m, 1, 2, 3, 0, 1, 3) -
    m[3] * minor(m, 1, 2, 3, 0, 1, 2);
}

template <unsigned long N, unsigned long M> float Matrix<N, M>::determinant(int n) {
    float det = 0;
    Matrix<N, M> submatrix;
    if (n == 2)
        return ((m[0][0] * m[1][1]) - (m[1][0] * m[0][1]));
    else {
        for (int x = 0; x < n; x++) {
            int subi = 0;
            for (int i = 1; i < n; i++) {
                int subj = 0;
                for (int j = 0; j < n; j++) {
                    if (j == x)
                        continue;
                    submatrix.m[subi][subj] = m[i][j];
                    subj++;
                }
                subi++;
            }
            det = det + (pow(-1.f, x) * m[0][x] * submatrix.determinant(n - 1));
        }
    }
    return det;
}

template <unsigned long N, unsigned long M>
Mat4_4 Matrix<N, M>::invert()
{
    int i=0,j=0;
    float y[3][3];
    auto c = Mat4_4::identity();
    
    // CALCULATION OF COFACTOR
    y[0][0]=m[1][1]*m[2][2]-m[1][2]*m[2][1];
    y[0][1]=-1*(m[1][0]*m[2][2]-m[1][2]*m[2][0]);
    y[0][2]=m[1][0]*m[2][1]-m[1][1]*m[2][0];
    y[1][0]=-1*(m[0][1]*m[2][2]-m[2][1]*m[0][2]);
    y[1][1]=m[0][0]*m[2][2]-m[0][2]*m[2][0];
    y[1][2]=-1*(m[0][0]*m[2][1]-m[0][1]*m[2][0]);
    y[2][0]=m[0][1]*m[1][2]-m[0][2]*m[1][1];
    y[2][1]=-1*(m[0][0]*m[1][2]-m[0][2]*m[1][0]);
    y[2][2]=m[0][0]*m[1][1]-m[0][1]*m[1][0];
    
    float det=m[0][0]*y[0][0]-m[0][1]*y[0][1]+m[0][2]*y[0][2];
    
    for(i=0;i<3;i++) //For storing adjoint in another 2-D Array
    {
        for(j=0;j<3;j++)
            c[i][j]=y[j][i];
    }
    
    if(fabs(det) >= EPS_VALUE)
    {
        for(i=0;i<3;i++) //For calculating inverse of matrix (2-D Array)
        {
            for(j=0;j<3;j++)
                c[i][j]=c[i][j]/det;
        }
    }
    else
    {
        std::cout<< "Determinant is zero";
    }
    
    return c;
}

#endif //__GEOMETRY_H__
