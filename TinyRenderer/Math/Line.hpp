//
//  Line.hpp
//  Tinyrenderer
//
//  Created by Aleksandr Borodulin on 28/02/2019.
//  Copyright © 2019 Aleksandr Borodulin. All rights reserved.
//

#ifndef Line_hpp
#define Line_hpp

#include "Math.h"

class Line {
    Vec3f point1;
    Vec3f point2;
};

#endif /* Line_hpp */
