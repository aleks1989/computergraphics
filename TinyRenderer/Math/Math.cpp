//
//  Math.cpp
//  ComputerGraphics
//
//  Created by Aleksandr Borodulin on 28/08/2019.
//  Copyright © 2019 Aleksandr Borodulin. All rights reserved.
//

#include "Math.h"

std::ostream& operator<<(std::ostream& s, Mat4_4& m) {
    for (int i=0; i<4; i++)  {
        for (int j=0; j<4; j++) {
            s << m[i][j];
            if (j<4) s << "\t";
        }
        s << "\n";
    }
    s << "\n";
    return s;
}
