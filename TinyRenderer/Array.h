//
//  Types.h
//  ComputerGraphics
//
//  Created by Aleksandr Borodulin on 02/07/2019.
//  Copyright © 2019 Aleksandr Borodulin. All rights reserved.
//

#ifndef Types_h
#define Types_h

#include <vector>

template <typename T>
using arr = std::vector<T>;


#endif /* Types_h */
