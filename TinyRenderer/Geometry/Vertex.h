//
//  Vertex.h
//  ComputerGraphics
//
//  Created by Aleksandr Borodulin on 10/06/2019.
//  Copyright © 2019 Aleksandr Borodulin. All rights reserved.
//

#ifndef Vertex_h
#define Vertex_h

#include "Math.h"

template <typename T>
struct Vertex {
    Vec4<T> position;
    Vec2f texCoord;
    
    template <typename U>
    inline Vertex<T>& operator= (Vertex<U> rhv) {
        position = rhv.position;
        texCoord = rhv.texCoord;
        return *this;
    }
};

template <typename T>
struct Triangle {
private:
#ifdef DEBUG
    static int idCounter;
#endif
public:
    Vertex<T> v0;
    Vertex<T> v1;
    Vertex<T> v2;
    Vec3<T> norm;
#ifdef DEBUG
    int identifier;
#endif
    Triangle();
    
    template <typename U>
    inline Triangle<T>& operator= (Triangle<U> rhv) {
        v0 = rhv.v0;
        v1 = rhv.v1;
        v2 = rhv.v2;
        
        norm = rhv.norm;
#ifdef DEBUG
        identifier = rhv.identifier;
#endif
        return *this;
    }
    
    inline Vertex<T>& operator[] (int index) {
        return ((Vertex<T>*)this)[index];
    }
};

template <typename T>
int Triangle<T>::idCounter;

template <typename T>
Triangle<T>::Triangle()
{
#ifdef DEBUG
    Triangle<T>::idCounter++;
    identifier = Triangle<T>::idCounter;
    
    if (identifier == 26)
    {
        std::cout << "Success";
    }
#endif
}

#endif /* Vertex_h */
