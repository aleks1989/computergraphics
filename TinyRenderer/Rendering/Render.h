//
//  render.hpp
//  Tinyrenderer
//
//  Created by Aleksandr Borodulin on 28/02/2019.
//  Copyright © 2019 Aleksandr Borodulin. All rights reserved.
//

#ifndef render_hpp
#define render_hpp

#include <map>
#include "Line.hpp"
#include "Camera.hpp"
#include "model.h"
#include <cmath>
#include "Memory.h"
#include "OCLVecMatMul.hpp"
#include "Vertex.h"
#include "BSPTree.h"

#define NEAR_CLIPPING_VALUE 1
//#define DRAW_BY_BBOX

const Vec3f view_dir(0,0,1);

std::map<int, float> randMap;
float randomNum = 0;

class Render {
private:
    union IntFloat {
        int integer;
        float floating;
    };
    
    TGAImage im;
    TGAImage* texture;
    float texWidth;
    float texHeight;
    
    IntFloat zBufVal;
    float *zBuffer;
    int bufSize;
    
    Camera* camera;
    
    Mat4_4 objMatrix;
    
    BSPTree* bsp;
    
    int triNumber;
#ifdef USE_OPEN_CL
    ocl_vmm_shr_ptr engine;
#endif
    
    inline void clipFront(Triangle<float>& face);
    inline void clipLeft(Triangle<float>& face);
    inline void clipRight(Triangle<float>& face);
    inline void clipBottom(Triangle<float>& face);
    inline void clipTop(Triangle<float>& face);
    inline void drawTriangle(Triangle<float>& face);
    
    inline void drawBBox(Triangle<float>& face);
    
    void renderBSPNode(BSPNode* node);
public:
    RGBA* renderBuffer;
    
    Render() = delete;
    Render(Camera* cam);
    
    ~Render();
    
    void resetBuffers();
    
    void cleanRenderBuffer();
    void cleanZBuffer();
    
    void line(int x0, int y0, int x1, int y1, RGBA color);
    void line(Vec2i p0, Vec2i p1, RGBA color);
    
    inline void renderObject(Model<float>* obj);
    void renderBSP(BSPTree* tree);
    
    int triangleId;
};

Render::Render(Camera* cam) : camera(cam), zBuffer(nullptr), renderBuffer(nullptr) {
    zBufVal.floating = 0.f;
    resetBuffers();
    
#ifdef USE_OPEN_CL
    engine = OCLVecMatMul::Create();
#endif
}

Render::~Render() {
    delete [] zBuffer;
    delete [] renderBuffer;
}

void Render::resetBuffers() {
    delete [] zBuffer;
    delete [] renderBuffer;
    
    bufSize = camera->screenWidth() * camera->screenHeight();
    zBuffer = new float[bufSize];
    renderBuffer = new RGBA[bufSize];
    
    cleanRenderBuffer();
    cleanZBuffer();
}

void Render::cleanRenderBuffer() {
    Mem_Set_Int(renderBuffer, 0xffffffff, bufSize);
}

void Render::cleanZBuffer() {
    Mem_Set_Int(zBuffer, zBufVal.integer, bufSize);
}

void Render::renderObject(Model<float>* obj) {
    triNumber = 0;
    triangleId = 0;
    
    texture = (obj->getTexture())->tga_image();
    texWidth = texture->get_width();
    texHeight = texture->get_height();
    
    objMatrix = camera->getProjMatrix() * obj->objMat; // Reverse order
    
    Triangle<float> tr;
    for (auto it = obj->faceBegin() ; it != obj->faceEnd(); ++it) {
        for (int j = 0; j < 3; j++) {
            FVertex fVert = (*it)[j];
            
            tr[j].texCoord = obj->texcoord(fVert.texIdx);
            tr[j].position = obj->vert(fVert.vertIdx);
        }
        
        auto matTest = objMatrix.transpose();
        
        auto mat = matTest.invert();
        
        Vec3f n = (tr[1].position - tr[0].position)^(tr[2].position - tr[0].position);
        n.normalize();
        
        auto normVec = Vec4f(n.x, n.y, n.z, 0);
        auto nt = mat * normVec;
        nt.normalize();
        
        for (int j = 0; j < 3; j++) {
#ifdef USE_OPEN_CL
            tri[j].position = engine->executeProgram(Vec4f(tr[j].position.x, tr[j].position.y, tr[j].position.z, 1.0), objMatrix);
#else
            tr[j].position = objMatrix * tr[j].position;
#endif
        }
        
        float hs = nt.x*(0.f - tr[0].position.x) + nt.y*(0.f - tr[0].position.y) + nt.z*(0.f - tr[0].position.z);
        if (hs < 0)
        {
            if (randMap.find((*it).identifier) == randMap.end())
            {
                randMap[(*it).identifier] = 0.7 + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(1.0-0.7)));
                randomNum = randMap[(*it).identifier];
            } else {
                randomNum = randMap[(*it).identifier];
            }
            clipFront(tr);
        }
    }
}

void Render::renderBSP(BSPTree* tree) {
    
    //bsp = tree; //delete
    
    auto node = (tree->getRoot());
    
    texture = (tree->getTexture())->tga_image();
    texWidth = texture->get_width();
    texHeight = texture->get_height();
    
    objMatrix = camera->getProjMatrix() * tree->objMat; // Reverse order
    
    renderBSPNode(node);
}

void Render::renderBSPNode(BSPNode* node) {
    auto faceCopy = node->face;
    Vec4f* p0 = &(faceCopy.v0.position);
    Vec4f* p1 = &(faceCopy.v1.position);
    Vec4f* p2 = &(faceCopy.v2.position);
    
    *(p0) = objMatrix * *(p0);
    *(p1) = objMatrix * *(p1);
    *(p2) = objMatrix * *(p2);
    
    auto matTest = objMatrix.transpose();
    
    auto mat = matTest.invert();
    
    auto normVec = Vec4f(node->face.norm.x, node->face.norm.y, node->face.norm.z, 0);
    auto n = mat * normVec;
    n.normalize();
    
    float hs = n.x*(0.f - p0->x) + n.y*(0.f - p0->y) + n.z*(0.f - p0->z);
    
    if (hs >= 0.f) { //Front
        if (node->back != nullptr) renderBSPNode(node->back);
        if (!node->complanarFaces.empty()) {
            for (auto it = node->complanarFaces.begin(); it != node->complanarFaces.end(); ++it) {
                Triangle<float> tr = *it;
                
                Vec4f* p0 = &(tr.v0.position);
                
                tr.v0.position = objMatrix * tr.v0.position;
                tr.v1.position = objMatrix * tr.v1.position;
                tr.v2.position = objMatrix * tr.v2.position;
                
                auto normVec = Vec4f(tr.norm.x, tr.norm.y, tr.norm.z, 0);
                auto n = mat * normVec;
                n.normalize();
                
                float hs = n.x*(0.f - p0->x) + n.y*(0.f - p0->y) + n.z*(0.f - p0->z);
                
                if (hs < 0.f) {
                    if (randMap.find(tr.identifier) == randMap.end())
                    {
                        randMap[tr.identifier] = 0.7 + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(1.0-0.7)));
                        randomNum = randMap[tr.identifier];
                    } else {
                        randomNum = randMap[tr.identifier];
                    }
                    clipFront(tr);
                }
            }
        }
        
        if (node->front != nullptr) renderBSPNode(node->front);
    } else {
        if (node->front != nullptr) renderBSPNode(node->front);
        if (!node->complanarFaces.empty()) {
            for (auto it = node->complanarFaces.begin(); it != node->complanarFaces.end(); ++it) {
                Triangle<float> tr = *it;
                
                Vec4f* p0 = &(tr.v0.position);
                
                tr.v0.position = objMatrix * tr.v0.position;
                tr.v1.position = objMatrix * tr.v1.position;
                tr.v2.position = objMatrix * tr.v2.position;
                
                auto normVec = Vec4f(tr.norm.x, tr.norm.y, tr.norm.z, 0);
                auto n = mat * normVec;
                n.normalize();
                
                float hs = n.x*(0.f - p0->x) + n.y*(0.f - p0->y) + n.z*(0.f - p0->z);
                
                if (hs < 0.f) {
                    if (randMap.find(tr.identifier) == randMap.end())
                    {
                        randMap[tr.identifier] = 0.7 + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(1.0-0.7)));
                        randomNum = randMap[tr.identifier];
                    } else {
                        randomNum = randMap[tr.identifier];
                    }
                    clipFront(tr);
                }
            }
        }
        if (randMap.find(faceCopy.identifier) == randMap.end())
        {
            randMap[faceCopy.identifier] = 0.7 + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(1.0-0.7)));
            randomNum = randMap[faceCopy.identifier];
        } else {
            randomNum = randMap[faceCopy.identifier];
        }
        clipFront(faceCopy);
        if (node->back != nullptr) renderBSPNode(node->back);
    }
}

void Render::clipFront(Triangle<float>& face) {
    
    //clip test for near plane
    
    if (face.identifier == 26) {
        //std::cout << "Success";
        //return;
    }
    
    Vertex<float>* clipPoints[2];
    Vertex<float>* notClipPoints[2];
    
    int clipCount = 0;
    int notClipCount = 0;
    
    for (int i = 0; i < 3; i++) {
        
        if (face[i].position.w < NEAR_CLIPPING_VALUE) {
            if (clipCount == 2) {
                return;
            }
            clipPoints[clipCount] = &(face[i]);
            clipCount += 1;
        } else {
            if (notClipCount == 2)
            {
                clipLeft(face);
                return;
            }
            notClipPoints[notClipCount] = &(face[i]);
            notClipCount += 1;
        }
    }
    
    //Polygon splitting not implemented
    
    if (clipCount == 2) {
        float dif1 = notClipPoints[0]->position.w - clipPoints[0]->position.w;
        float dif2 = notClipPoints[0]->position.w - clipPoints[1]->position.w;
        
        if (dif1 > EPS_VALUE && dif2 > EPS_VALUE) {
            float alpha1 =  (NEAR_CLIPPING_VALUE - clipPoints[0]->position.w) / dif1;
            float alpha2 =  (NEAR_CLIPPING_VALUE - clipPoints[1]->position.w) / dif2;
            
            clipPoints[0]->position = clipPoints[0]->position + (notClipPoints[0]->position - clipPoints[0]->position) * alpha1;
            clipPoints[1]->position = clipPoints[1]->position + (notClipPoints[0]->position - clipPoints[1]->position) * alpha2;
            clipPoints[0]->texCoord = clipPoints[0]->texCoord + (notClipPoints[0]->texCoord - clipPoints[0]->texCoord) * alpha1;
            clipPoints[1]->texCoord = clipPoints[1]->texCoord + (notClipPoints[0]->texCoord - clipPoints[1]->texCoord) * alpha2;
            clipLeft(face);
        }
        
    } else {
        float dif1 = notClipPoints[0]->position.w - clipPoints[0]->position.w;
        float dif2 = notClipPoints[1]->position.w - clipPoints[0]->position.w;
        
        Vertex<float> clipPoint_0 = *(clipPoints[0]);
        
        if (dif2 > EPS_VALUE) {
            float alpha =  (NEAR_CLIPPING_VALUE - clipPoint_0.position.w) / dif2;
            clipPoints[0]->position = clipPoint_0.position + (notClipPoints[1]->position - clipPoint_0.position) * alpha;
            clipPoints[0]->texCoord = clipPoint_0.texCoord + (notClipPoints[1]->texCoord - clipPoint_0.texCoord) * alpha;
            
            Triangle<float> tr = face;
            clipLeft(tr);
        }
        
        if (dif1 > EPS_VALUE) {
            
            float alpha =  (NEAR_CLIPPING_VALUE - clipPoint_0.position.w) / dif1;
            *(notClipPoints[1]) = *(notClipPoints[0]);
            notClipPoints[0]->position = clipPoint_0.position + (notClipPoints[0]->position - clipPoint_0.position) * alpha;
            notClipPoints[0]->texCoord = clipPoint_0.texCoord + (notClipPoints[0]->texCoord - clipPoint_0.texCoord) * alpha;
            
            clipLeft(face);
        }
    }
}

void Render::clipLeft(Triangle<float>& face) {
    Vertex<float>* clipPoints[2];
    Vertex<float>* notClipPoints[2];
    
    int clipCount = 0;
    int notClipCount = 0;
    
    Vertex<float> * end = &(face.v2);
    for (Vertex<float> * it = &(face.v0); it <= end; ++it) {
        
        if (it->position.x < -(it->position.w)) {
            if (clipCount == 2) {
                return;
            }
            clipPoints[clipCount] = it;
            clipCount += 1;
        } else {
            if (notClipCount == 2)
            {
                clipRight(face);
                return;
            }
            notClipPoints[notClipCount] = it;
            notClipCount += 1;
        }
    }
    
    //Polygon splitting not implemented
    
    if (clipCount == 2) {
        float dif1 = (clipPoints[0]->position.w + clipPoints[0]->position.x) - (notClipPoints[0]->position.w + notClipPoints[0]->position.x);
        float dif2 = (clipPoints[1]->position.w + clipPoints[1]->position.x) - (notClipPoints[0]->position.w + notClipPoints[0]->position.x);
        
        if (std::fabs(dif1) > EPS_VALUE && std::fabs(dif2) > EPS_VALUE) {
            float alpha1 =  (clipPoints[0]->position.w + clipPoints[0]->position.x) / dif1;
            float alpha2 =  (clipPoints[1]->position.w + clipPoints[1]->position.x) / dif2;
            
            //Vec4f test = (notClipPoints[0]->position - clipPoints[0]->position) * alpha1;
            
            clipPoints[0]->position = clipPoints[0]->position + (notClipPoints[0]->position - clipPoints[0]->position) * alpha1;
            clipPoints[1]->position = clipPoints[1]->position + (notClipPoints[0]->position - clipPoints[1]->position) * alpha2;
            clipPoints[0]->texCoord = clipPoints[0]->texCoord + (notClipPoints[0]->texCoord - clipPoints[0]->texCoord) * alpha1;
            clipPoints[1]->texCoord = clipPoints[1]->texCoord + (notClipPoints[0]->texCoord - clipPoints[1]->texCoord) * alpha2;
            clipRight(face);
        }
        
    } else {
        float dif1 = (clipPoints[0]->position.w + clipPoints[0]->position.x) - (notClipPoints[0]->position.w + notClipPoints[0]->position.x);
        float dif2 = (clipPoints[0]->position.w + clipPoints[0]->position.x) - (notClipPoints[1]->position.w + notClipPoints[1]->position.x);
        
        Vertex<float> clipPoint_0 = *(clipPoints[0]);
        
        if (std::fabs(dif2) > EPS_VALUE) {
            float alpha =  (clipPoint_0.position.w + clipPoint_0.position.x) / dif2;
            clipPoints[0]->position = clipPoint_0.position + (notClipPoints[1]->position - clipPoint_0.position) * alpha;
            clipPoints[0]->texCoord = clipPoint_0.texCoord + (notClipPoints[1]->texCoord - clipPoint_0.texCoord) * alpha;
            
            Triangle<float> tr = face;
            clipRight(tr);
        }
        
        if (std::fabs(dif1) > EPS_VALUE) {
            
            float alpha =  (clipPoint_0.position.w + clipPoint_0.position.x) / dif1;
            *(notClipPoints[1]) = *(notClipPoints[0]);
            notClipPoints[0]->position = clipPoint_0.position + (notClipPoints[0]->position - clipPoint_0.position) * alpha;
            notClipPoints[0]->texCoord = clipPoint_0.texCoord + (notClipPoints[0]->texCoord - clipPoint_0.texCoord) * alpha;
            clipRight(face);
        }
    }
}

void Render::clipRight(Triangle<float>& face) {
    Vertex<float>* clipPoints[2];
    Vertex<float>* notClipPoints[2];
    
    int clipCount = 0;
    int notClipCount = 0;
    
    Vertex<float> * end = &(face.v2);
    for (Vertex<float> * it = &(face.v0); it <= end; ++it) {
        
        if (it->position.x > it->position.w) {
            if (clipCount == 2) {
                return;
            }
            clipPoints[clipCount] = it;
            clipCount += 1;
        } else {
            if (notClipCount == 2)
            {
                //clipRight(vertices);
                clipBottom(face);
                return;
            }
            notClipPoints[notClipCount] = it;
            notClipCount += 1;
        }
    }
    
    //Polygon splitting not implemented
    
    if (clipCount == 2) {
        float dif1 = (notClipPoints[0]->position.x - notClipPoints[0]->position.w) - (clipPoints[0]->position.x - clipPoints[0]->position.w);
        float dif2 = (notClipPoints[0]->position.x - notClipPoints[0]->position.w) - (clipPoints[1]->position.x - clipPoints[1]->position.w);
        
        if (std::fabs(dif1) > EPS_VALUE && std::fabs(dif2) > EPS_VALUE) {
            float alpha1 =  (notClipPoints[0]->position.x - notClipPoints[0]->position.w) / dif1;
            float alpha2 =  (notClipPoints[0]->position.x - notClipPoints[0]->position.w) / dif2;
            
            clipPoints[0]->position = notClipPoints[0]->position + (clipPoints[0]->position - notClipPoints[0]->position) * alpha1;
            clipPoints[1]->position = notClipPoints[0]->position + (clipPoints[1]->position - notClipPoints[0]->position) * alpha2;
            clipPoints[0]->texCoord = notClipPoints[0]->texCoord + (clipPoints[0]->texCoord - notClipPoints[0]->texCoord) * alpha1;
            clipPoints[1]->texCoord = notClipPoints[0]->texCoord + (clipPoints[1]->texCoord - notClipPoints[0]->texCoord) * alpha2;
            //clipRight(vertices);
            clipBottom(face);
        }
        
    } else {
        float dif1 = (notClipPoints[0]->position.x - notClipPoints[0]->position.w) - (clipPoints[0]->position.x - clipPoints[0]->position.w);
        float dif2 = (notClipPoints[1]->position.x - notClipPoints[1]->position.w) - (clipPoints[0]->position.x - clipPoints[0]->position.w);
        
        Vertex<float> clipPoint_0 = *(clipPoints[0]);
        
        if (std::fabs(dif2) > EPS_VALUE) {
            float alpha = (notClipPoints[1]->position.x - notClipPoints[1]->position.w) / dif2;
            clipPoints[0]->position = notClipPoints[1]->position + (clipPoint_0.position - notClipPoints[1]->position) * alpha;
            clipPoints[0]->texCoord = notClipPoints[1]->texCoord + (clipPoint_0.texCoord - notClipPoints[1]->texCoord) * alpha;
            
            Triangle<float> tr = face;
            clipBottom(tr);
        }
        
        if (std::fabs(dif1) > EPS_VALUE) {
            float alpha = (notClipPoints[0]->position.x - notClipPoints[0]->position.w) / dif1;
            *(notClipPoints[1]) = *(notClipPoints[0]);
            notClipPoints[0]->position = notClipPoints[0]->position + (clipPoint_0.position - notClipPoints[0]->position) * alpha;
            notClipPoints[0]->texCoord = notClipPoints[0]->texCoord + (clipPoint_0.texCoord - notClipPoints[0]->texCoord) * alpha;
            clipBottom(face);
        }
    }
}

void Render::clipBottom(Triangle<float>& face) {
    Vertex<float>* clipPoints[2];
    Vertex<float>* notClipPoints[2];
    
    int clipCount = 0;
    int notClipCount = 0;
    
    Vertex<float> * end = &(face.v2);
    for (Vertex<float> * it = &(face.v0); it <= end; ++it) {
        
        if (it->position.y < -(it->position.w)) {
            if (clipCount == 2) {
                return;
            }
            clipPoints[clipCount] = it;
            clipCount += 1;
        } else {
            if (notClipCount == 2)
            {
                clipTop(face);
                return;
            }
            notClipPoints[notClipCount] = it;
            notClipCount += 1;
        }
    }
    
    //Polygon splitting not implemented
    
    if (clipCount == 2) {
        float dif1 = (clipPoints[0]->position.w + clipPoints[0]->position.y) - (notClipPoints[0]->position.w + notClipPoints[0]->position.y);
        float dif2 = (clipPoints[1]->position.w + clipPoints[1]->position.y) - (notClipPoints[0]->position.w + notClipPoints[0]->position.y);
        
        if (std::fabs(dif1) > EPS_VALUE && std::fabs(dif2) > EPS_VALUE) {
            float alpha1 =  (clipPoints[0]->position.w + clipPoints[0]->position.y) / dif1;
            float alpha2 =  (clipPoints[1]->position.w + clipPoints[1]->position.y) / dif2;
            
            //Vec4f test = (notClipPoints[0]->position - clipPoints[0]->position) * alpha1;
            
            clipPoints[0]->position = clipPoints[0]->position + (notClipPoints[0]->position - clipPoints[0]->position) * alpha1;
            clipPoints[1]->position = clipPoints[1]->position + (notClipPoints[0]->position - clipPoints[1]->position) * alpha2;
            clipPoints[0]->texCoord = clipPoints[0]->texCoord + (notClipPoints[0]->texCoord - clipPoints[0]->texCoord) * alpha1;
            clipPoints[1]->texCoord = clipPoints[1]->texCoord + (notClipPoints[0]->texCoord - clipPoints[1]->texCoord) * alpha2;
            clipTop(face);
        }
        
    } else {
        float dif1 = (clipPoints[0]->position.w + clipPoints[0]->position.y) - (notClipPoints[0]->position.w + notClipPoints[0]->position.y);
        float dif2 = (clipPoints[0]->position.w + clipPoints[0]->position.y) - (notClipPoints[1]->position.w + notClipPoints[1]->position.y);
        
        Vertex<float> clipPoint_0 = *(clipPoints[0]);
        
        if (std::fabs(dif2) > EPS_VALUE) {
            
            float alpha = (clipPoint_0.position.w + clipPoint_0.position.y) / dif2;
            clipPoints[0]->position = clipPoint_0.position + (notClipPoints[1]->position - clipPoint_0.position) * alpha;
            clipPoints[0]->texCoord = clipPoint_0.texCoord + (notClipPoints[1]->texCoord - clipPoint_0.texCoord) * alpha;
            
            Triangle<float> tr = face;
            clipTop(tr);
        }
        
        if (std::fabs(dif1) > EPS_VALUE) {
            
            float alpha =  (clipPoint_0.position.w + clipPoint_0.position.y) / dif1;
            *(notClipPoints[1]) = *(notClipPoints[0]);
            notClipPoints[0]->position = clipPoint_0.position + (notClipPoints[0]->position - clipPoint_0.position) * alpha;
            notClipPoints[0]->texCoord = clipPoint_0.texCoord + (notClipPoints[0]->texCoord - clipPoint_0.texCoord) * alpha;
            clipTop(face);
        }
    }
}

void Render::clipTop(Triangle<float>& face) {
    Vertex<float>* clipPoints[2];
    Vertex<float>* notClipPoints[2];
    
    int clipCount = 0;
    int notClipCount = 0;
    
    Vertex<float> * end = &(face.v2);
    for (Vertex<float> * it = &(face.v0); it <= end; ++it) {
        
        if (it->position.y > it->position.w) {
            if (clipCount == 2) {
                return;
            }
            clipPoints[clipCount] = it;
            clipCount += 1;
        } else {
            if (notClipCount == 2)
            {
                //clipRight(vertices);
                drawTriangle(face);
                return;
            }
            notClipPoints[notClipCount] = it;
            notClipCount += 1;
        }
    }
    
    //Polygon splitting not implemented
    
    if (clipCount == 2) {
        float dif1 = (notClipPoints[0]->position.y - notClipPoints[0]->position.w) - (clipPoints[0]->position.y - clipPoints[0]->position.w);
        float dif2 = (notClipPoints[0]->position.y - notClipPoints[0]->position.w) - (clipPoints[1]->position.y - clipPoints[1]->position.w);
        
        if (std::fabs(dif1) > EPS_VALUE && std::fabs(dif2) > EPS_VALUE) {
            float alpha1 =  (notClipPoints[0]->position.y - notClipPoints[0]->position.w) / dif1;
            float alpha2 =  (notClipPoints[0]->position.y - notClipPoints[0]->position.w) / dif2;
            
            clipPoints[0]->position = notClipPoints[0]->position + (clipPoints[0]->position - notClipPoints[0]->position) * alpha1;
            clipPoints[1]->position = notClipPoints[0]->position + (clipPoints[1]->position - notClipPoints[0]->position) * alpha2;
            clipPoints[0]->texCoord = notClipPoints[0]->texCoord + (clipPoints[0]->texCoord - notClipPoints[0]->texCoord) * alpha1;
            clipPoints[1]->texCoord = notClipPoints[0]->texCoord + (clipPoints[1]->texCoord - notClipPoints[0]->texCoord) * alpha2;
            //clipRight(vertices);
            drawTriangle(face);
        }
        
    } else {
        float dif1 = (notClipPoints[0]->position.y - notClipPoints[0]->position.w) - (clipPoints[0]->position.y - clipPoints[0]->position.w);
        float dif2 = (notClipPoints[1]->position.y - notClipPoints[1]->position.w) - (clipPoints[0]->position.y - clipPoints[0]->position.w);
        
        Vertex<float> clipPoint_0 = *(clipPoints[0]);
        
        if (std::fabs(dif2) > EPS_VALUE) {
            
            float alpha = (notClipPoints[1]->position.y - notClipPoints[1]->position.w) / dif2;
            clipPoints[0]->position = notClipPoints[1]->position + (clipPoint_0.position - notClipPoints[1]->position) * alpha;
            clipPoints[0]->texCoord = notClipPoints[1]->texCoord + (clipPoint_0.texCoord - notClipPoints[1]->texCoord) * alpha;
            
            Triangle<float> tr = face;
            drawTriangle(tr);
        }
        
        if (std::fabs(dif1) > EPS_VALUE) {
            float alpha = (notClipPoints[0]->position.y - notClipPoints[0]->position.w) / dif1;
            *(notClipPoints[1]) = *(notClipPoints[0]);
            notClipPoints[0]->position = notClipPoints[0]->position + (clipPoint_0.position - notClipPoints[0]->position) * alpha;
            notClipPoints[0]->texCoord = notClipPoints[0]->texCoord + (clipPoint_0.texCoord - notClipPoints[0]->texCoord) * alpha;
            drawTriangle(face);
        }
    }
}

void Render::drawTriangle(Triangle<float>& face) {
    
    triNumber++;
    if (triNumber == 2) {
        //return;
        //std::cout << "Success";
    } else {
        //return;
    }
#ifdef DRAW_BY_BBOX
    drawBBox(face);
#else
    Vec4f p0_h = face.v0.position;
    Vec4f p1_h = face.v1.position;
    Vec4f p2_h = face.v2.position;
    
    p0_h.w = 1.f / p0_h.w;
    p1_h.w = 1.f / p1_h.w;
    p2_h.w = 1.f / p2_h.w;
    
    p0_h.x = (p0_h.x * camera->half_screenWidth_f_mho() + camera->half_screenWidth_f_mho() * face.v0.position.w) * p0_h.w;
    p0_h.y = (p0_h.y * camera->half_screenHeight_f_mho() + camera->half_screenHeight_f_mho() * face.v0.position.w) * p0_h.w;
    
    p1_h.x = (p1_h.x * camera->half_screenWidth_f_mho() + camera->half_screenWidth_f_mho() * face.v1.position.w) * p1_h.w;
    p1_h.y = (p1_h.y * camera->half_screenHeight_f_mho() + camera->half_screenHeight_f_mho() * face.v1.position.w) * p1_h.w;
    
    p2_h.x = (p2_h.x * camera->half_screenWidth_f_mho() + camera->half_screenWidth_f_mho() * face.v2.position.w) * p2_h.w;
    p2_h.y = (p2_h.y * camera->half_screenHeight_f_mho() + camera->half_screenHeight_f_mho() * face.v2.position.w) * p2_h.w;
    /*
    Vec3f n = (p1_h - p0_h)^(p2_h - p0_h);
    n.normalize();
    
    float visibility = n * view_dir;
    
    if (visibility <= 0.f) {
        return;
    }*/
    
    if (p0_h.y > p1_h.y) {
        std::swap(p0_h, p1_h);
        std::swap(face.v0, face.v1);
    }
    if (p0_h.y > p2_h.y) {
        std::swap(p0_h, p2_h);
        std::swap(face.v0, face.v2);
    }
    if (p1_h.y > p2_h.y) {
        std::swap(p1_h, p2_h);
        std::swap(face.v1, face.v2);
    }
    
    Vec2f* uv0 = &face.v0.texCoord;
    Vec2f* uv1 = &face.v1.texCoord;
    Vec2f* uv2 = &face.v2.texCoord;
    
    // Rasterize triangle
    
    if (p0_h.y < 0.f) {
        p0_h.y = 0.f;
    }
    
    if (p1_h.y < 0.f) {
        p1_h.y = 0.f;
    }
    
    Vec2f p0 = Vec2f(floor(p0_h.x), floor(p0_h.y));
    Vec2f p1 = Vec2f(floor(p1_h.x), floor(p1_h.y));
    Vec2f p2 = Vec2f(floor(p2_h.x), floor(p2_h.y));
    
    const Vec2f total_dif = p2 - p0;
    
    if (total_dif.y < EPS_VALUE) {
        return;
    }
    
    float inv_y = 1.f / total_dif.y;
    
    float segment_dif_y = p1.y - p0.y;
    
    float p1_opp_weight = segment_dif_y * inv_y;
    
    float p1_opp_w = p0_h.w + (p2_h.w - p0_h.w) * p1_opp_weight;
    Vec2f p1_opp_uv = (((*uv0)*p0_h.w)*(1.f - p1_opp_weight) + ((*uv2)*p2_h.w)*(p1_opp_weight)) / p1_opp_w;
    
    if (inv_y > 1.f) {
        inv_y = 1.f;
    }
    
    float alpha = 0.f;
    int y = p0.y;
    
    float d = Dot2(p0, p2, p1);
    
    bool swaped = (d < 0.f) ? true : false; //Need to write code for each case to reduce conditions in code
    
    if (segment_dif_y > EPS_VALUE) {
        float d_beta = (segment_dif_y < 1.f) ? 1.f : 1.f / segment_dif_y;
        float beta = 0.f;
        
        float segment_dif_x = p1.x - p0.x;
        
        if (y < 0) {
            y = 0;
        }
        
        for (;y < int(p1.y); y++, alpha += inv_y, beta += d_beta) {
            if (y < camera->screenHeight()) {
                float a_x = p0.x + total_dif.x * alpha;
                float b_x = p0.x + segment_dif_x * beta;
                
                if (swaped) { std::swap(a_x, b_x); }
                
                float xDif = b_x - a_x;
                
                float d_beta_phi = (xDif < 1.f) ? beta : beta / xDif;
                float beta_phi = 0.f;
                
                int j = a_x;
                float a = (1.f - beta) * p0_h.w;
                Vec2f uv0_corr = (*uv0) * a;
                if (j < 0) {
                    j = 0;
                }
                
                if (b_x >= camera->screenWidth()) {
                    b_x = camera->screenWidth() - 1;
                }
                
                int idx = j + y * camera->screenWidth();
                
                for (;j <= b_x; j++, beta_phi += d_beta_phi) {
                    if (idx >= 0 && idx < bufSize) {
                        float barCoord_Y = beta_phi;
                        float barCoord_Z = beta - barCoord_Y;
                        
                        if (swaped) {
                            std::swap(barCoord_Y, barCoord_Z);
                        }
                        
                        float b = barCoord_Y * p1_h.w;
                        float c = barCoord_Z * p1_opp_w;
                        
                        float inv_z = (a + b + c);
                        if (bsp != nullptr || inv_z > zBuffer[idx]) {
                            zBuffer[idx] = inv_z;
                            if (texture != nullptr) {
                                Vec2f uvP = (uv0_corr + (*uv1) * b  + p1_opp_uv * c) / inv_z;
                                renderBuffer[idx] = texture->get(uvP.x, uvP.y);
                                /*renderBuffer[idx].r *= randomNum;
                                renderBuffer[idx].g *= randomNum;
                                renderBuffer[idx].b *= randomNum;*/
                            } else {
                                renderBuffer[idx] = RGBA(0xFF000000);
                            }
                        }
                    }
                    idx++;
                }
            }
        }
    }
    
    if (y < p2.y) {
        Vec2f segment_dif = p2 - p1;
        
        float d_beta = (segment_dif.y < 1.f) ? 1.f : 1.f / segment_dif.y;
        float beta = 0.f;
        
        if (y < 0) {
            y = 0;
        }
        
        for (;y <= int(p2.y); y++, alpha += inv_y, beta += d_beta) {
            if (y < camera->screenHeight()) {
                float a_x = p0.x + total_dif.x * alpha;
                float b_x = p1.x + segment_dif.x * beta;
                
                if (swaped) { std::swap(a_x, b_x);}
                
                float xDif = b_x - a_x;
                
                int j = a_x;
                
                float one_min_beta = 1 - beta;
                
                float d_beta_phi = (xDif < 1.f) ? one_min_beta : one_min_beta / xDif; //Need compare with 2
                float beta_phi = 0;
                
                float b = beta * p2_h.w;
                Vec2f uv2_corr = (*uv2) * b;
                
                if (j < 0) {
                    j = 0;
                }
                
                if (b_x >= camera->screenWidth()) {
                    b_x = camera->screenWidth() - 1;
                }
                
                int idx = j + y * camera->screenWidth();
                for (;j <= int(b_x); j++, beta_phi += d_beta_phi) {
                    if (idx >= 0 && idx < bufSize) {
                        float barCoord_Z = beta_phi;
                        float barCoord_X = one_min_beta - beta_phi;
                        
                        if (swaped) {
                            std::swap(barCoord_X, barCoord_Z);
                        }
                        
                        float a = barCoord_X * p1_opp_w;
                        float c = barCoord_Z * p1_h.w;
                        
                        float inv_z = (a + b + c);
                        if (bsp != nullptr || inv_z > zBuffer[idx]) {
                            zBuffer[idx] = inv_z;
                            if (texture != nullptr) {
                                Vec2f uvP = (p1_opp_uv * a + uv2_corr + (*uv1) * c) / inv_z;
                                renderBuffer[idx] = texture->get(uvP.x, uvP.y);
                                /*renderBuffer[idx].r *= randomNum;
                                renderBuffer[idx].g *= randomNum;
                                renderBuffer[idx].b *= randomNum;*/
                            } else {
                                renderBuffer[idx] = RGBA(0xFF000000);
                            }
                        }
                    }
                    idx++;
                }
            }
        }
    }
#endif
}

void Render::drawBBox(Triangle<float>& face) {
    // Convert homogeneous coordinate to 3d
    
    Vec4f p0_h = face.v0.position;
    Vec4f p1_h = face.v1.position;
    Vec4f p2_h = face.v2.position;
    
    p0_h.w = 1 / p0_h.w;
    p1_h.w = 1 / p1_h.w;
    p2_h.w = 1 / p2_h.w;
    
    p0_h.x = (p0_h.x * camera->half_screenWidth_f_mho() + camera->half_screenWidth_f_mho() * face.v0.position.w) * p0_h.w;
    p0_h.y = (p0_h.y * camera->half_screenHeight_f_mho() + camera->half_screenHeight_f_mho() * face.v0.position.w) * p0_h.w;
    
    p1_h.x = (p1_h.x * camera->half_screenWidth_f_mho() + camera->half_screenWidth_f_mho() * face.v1.position.w) * p1_h.w;
    p1_h.y = (p1_h.y * camera->half_screenHeight_f_mho() + camera->half_screenHeight_f_mho() * face.v1.position.w) * p1_h.w;
    
    p2_h.x = (p2_h.x * camera->half_screenWidth_f_mho() + camera->half_screenWidth_f_mho() * face.v2.position.w) * p2_h.w;
    p2_h.y = (p2_h.y * camera->half_screenHeight_f_mho() + camera->half_screenHeight_f_mho() * face.v2.position.w) * p2_h.w;
    /*
    Vec3f n = (p1_h - p0_h)^(p2_h - p0_h);
    n.normalize();
    
    float visibility = n * view_dir;
    
    if (visibility <= 0.f) {
        return;
    }
    */
    Vec2df uv0 = Vec2df(face.v0.texCoord.x, face.v0.texCoord.y);
    Vec2df uv1 = Vec2df(face.v1.texCoord.x, face.v1.texCoord.y);
    Vec2df uv2 = Vec2df(face.v2.texCoord.x, face.v2.texCoord.y);
    
    RGBA color;
    color.value = 0;
    
    // Rasterize triangle
    
    Vec3df p0 = Vec3df(double(p0_h.x), double(p0_h.y), double(p0_h.z));
    Vec3df p1 = Vec3df(double(p1_h.x), double(p1_h.y), double(p1_h.z));
    Vec3df p2 = Vec3df(double(p2_h.x), double(p2_h.y), double(p2_h.z));
    
    int y_max = p2.y;
    
    if (p0.y > y_max) {
        y_max = p0.y;
    }
    
    if (p1.y > y_max) {
        y_max = p1.y;
    }
    
    int y_min = p0.y;
    
    if (p1.y < y_min) {
        y_min = p1.y;
    }
    
    if (p2.y < y_min) {
        y_min = p2.y;
    }
    
    int x_min = p0.x;
    
    if (p1.x < x_min) {
        x_min = p1.x;
    }
    
    if (p2.x < x_min) {
        x_min = p2.x;
    }
    
    int x_max = p2.x;
    
    if (p0.x > x_max) {
        x_max = p0.x;
    }
    
    if (p1.x > x_max) {
        x_max = p1.x;
    }
    
    double inv_area = 1.f / Dot(p0, p2, p1);//(p1_h - p0_h)^(p2_h - p0_h);
    
    for (int y = y_min; y <= y_max; y++) {
        for (int x = x_min; x <= x_max; x++) {
            Vec3df P = Vec3df(x, y, 0.f);
            double alpha = fabs(Dot(p1, p2, P) * inv_area);
            double beta = fabs(Dot(p2, p0, P) * inv_area);
            double phi = fabs(Dot(p0, p1, P) * inv_area);
            double baric_sum = alpha + beta + phi;
            if (alpha >= 0.f && alpha >= 0.f && phi >= 0.f && baric_sum <= 1.f) {
                
                double a = alpha * p0_h.w;
                double b = beta * p1_h.w;
                double c = phi * p2_h.w;
                
                float inv_z = (a + b + c);
                
                int idx = x + y * camera->screenWidth();
                
                if (inv_z > zBuffer[idx]) {
                    zBuffer[idx] = inv_z;
                    if (texture != nullptr) {
                        Vec2df uvP = (uv0 * a + uv1 * b  + uv2 * c) / inv_z;
                        
                        renderBuffer[idx] = texture->get(uvP.x, uvP.y);
                    } else {
                        renderBuffer[idx] = RGBA(0xFF000000);
                    }
                }
            }
        }
    }
}

#endif /* render_hpp */
