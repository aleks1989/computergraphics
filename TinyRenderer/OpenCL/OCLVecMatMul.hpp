//
//  OCLVecMatMul.h
//  ComputerGraphics
//
//  Created by Aleksandr Borodulin on 20/05/2019.
//  Copyright © 2019 Aleksandr Borodulin. All rights reserved.
//

#ifndef OCLVecMatMul_hpp
#define OCLVecMatMul_hpp

#if defined(USE_OPEN_CL)

#include "OCLObject.hpp"
#include "Math.h"

class OCLVecMatMul;

typedef std::shared_ptr<OCLVecMatMul> ocl_vmm_shr_ptr;

class OCLVecMatMul: public OCLObject
{
private:
    cl_mem mat_buff, vec_buff, res_buff;
protected:
    OCLVecMatMul();
public:
    static ocl_vmm_shr_ptr Create();
    
    Vec4f executeProgram(Vec4f vec, Matrix mat);
    
    ~OCLVecMatMul();
};

OCLVecMatMul::OCLVecMatMul() {
    
}

OCLVecMatMul::~OCLVecMatMul() {
    clReleaseKernel(kernel);
    clReleaseCommandQueue(queue);
    clReleaseProgram(program);
}

ocl_vmm_shr_ptr OCLVecMatMul::Create() {
    ocl_vmm_shr_ptr ptr = ocl_vmm_shr_ptr(new OCLVecMatMul());
    
    if (ptr->err != CL_SUCCESS) {
        return nullptr;
    }
    
    size_t program_size = strlen(matvec);
    
    
    ptr->program = clCreateProgramWithSource(ptr->context, 1, (const char**)&matvec, &program_size, &ptr->err);
    
    if (ptr->err != CL_SUCCESS) {
        return nullptr;
    }
    
    const char options[] = "-cl-std=CL1.1";
    clBuildProgram(ptr->program, 0, NULL, options, NULL, NULL);
    
    if (ptr->err != CL_SUCCESS) {
        size_t len = 0;
        ptr->err = clGetProgramBuildInfo(ptr->program, ptr->device, CL_PROGRAM_BUILD_LOG, 0, NULL, &len);
        char *buffer = (char *)calloc(len, sizeof(char));
        ptr->err = clGetProgramBuildInfo(ptr->program, ptr->device, CL_PROGRAM_BUILD_LOG, len, buffer, NULL);
        
        if (ptr->err == CL_SUCCESS) {
            printf("%s\n", buffer);
        }
        
        exit(1);
        free(buffer);
    }
    
    
    ptr->kernel = clCreateKernel(ptr->program, "matvec_mult", &ptr->err);
    if (ptr->err != CL_SUCCESS) {
        return nullptr;
    }
    
    ptr->queue = clCreateCommandQueue(ptr->context, ptr->device, 0, &ptr->err);
    if (ptr->err != CL_SUCCESS) {
        return nullptr;
    }
    
    ///////////////////////////////////////////////////////////////////////////////
    
    return ptr;
}

Vec4f OCLVecMatMul::executeProgram(Vec4f vec, Matrix mat) {
    Vec4f result;
    
    size_t work_units_per_kernel;
    
    mat_buff = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, sizeof(float) * 16, mat.raw, &err);
    
    vec_buff = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, sizeof(float) * 4, vec.raw, &err);
    
    res_buff = clCreateBuffer(context, CL_MEM_WRITE_ONLY, sizeof(float) * 4, NULL, &err);
    
    clSetKernelArg(kernel, 0, sizeof(cl_mem), &mat_buff);
    clSetKernelArg(kernel, 1, sizeof(cl_mem), &vec_buff);
    clSetKernelArg(kernel, 2, sizeof(cl_mem), &res_buff);
    
    work_units_per_kernel = 4;
    clEnqueueNDRangeKernel(queue, kernel, 1, NULL, &work_units_per_kernel, NULL, 0, NULL, NULL);
    
    clEnqueueReadBuffer(queue, res_buff, CL_TRUE, 0, sizeof(float) * 4, result.raw, 0, NULL, NULL);
    
    clReleaseMemObject(mat_buff);
    clReleaseMemObject(vec_buff);
    clReleaseMemObject(res_buff);
    
    return result;
}

#endif

#endif /* OCLVecMatMul_h */
