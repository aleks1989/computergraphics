//
//  OCLObject.hpp
//  ComputerGraphics
//
//  Created by Aleksandr Borodulin on 17/05/2019.
//  Copyright © 2019 Aleksandr Borodulin. All rights reserved.
//

#ifndef OCLObject_hpp
#define OCLObject_hpp

#if defined(USE_OPEN_CL)

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>

#ifdef MAC_OS
#include <OpenCL/cl.h>
#else
#include <CL/cl.h>
#endif

#include <iostream>
#include <fstream>

#define STRINGIFY(A) #A
#include "matvec.cl"

class OCLObject {
private:
protected:
    cl_device_id device;
    cl_context context;
    cl_int err;
    cl_program program;
    
    cl_command_queue queue;
    cl_kernel kernel;
    
    OCLObject();
    
    void contextRefCount();
public:
    ~OCLObject();
};

OCLObject::OCLObject() {
    cl_platform_id platform;
    
    clGetPlatformIDs(1, &platform, NULL);
    /*
    char pform_version[40];
    clGetPlatformInfo(platform, CL_PLATFORM_VERSION, sizeof(pform_version), &pform_version, NULL);*/
    
    clGetDeviceIDs(platform, CL_DEVICE_TYPE_GPU, 1, &device, NULL);
    context = clCreateContext(NULL, 1, &device, NULL, NULL, &err);
    
    if (err < 0) {
        perror("Couldn't create context.");
        exit(1);
    }
    
    contextRefCount();
}

OCLObject::~OCLObject() {
    clReleaseContext(context);
}

void OCLObject::contextRefCount() {
    cl_uint ref_count;
    err = clGetContextInfo(context, CL_CONTEXT_REFERENCE_COUNT, sizeof(ref_count), &ref_count, NULL);
    if (err < 0) {
        perror("Couldn't read the reference count.");
    }
    printf("Reference count: %u\n", ref_count);
}

#endif

#endif /* OCLObject_hpp */
