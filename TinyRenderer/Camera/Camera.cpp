//
//  Camera.cpp
//  ComputerGraphics
//
//  Created by Aleksandr Borodulin on 29/03/2019.
//  Copyright © 2019 Aleksandr Borodulin. All rights reserved.
//

#include "Camera.hpp"
#include <cassert>

Camera::Camera(int nClip, int fClip, int screenWidth, int screenHeight) : nearClip(nClip), farClip(fClip)
{
    projDist = nearClip;
    
    assert(projDist > 0 && nearClip > 0 && nClip < fClip);
    
    setScreenSize(screenWidth, screenHeight);
    
    clipSpace = farClip - nearClip;
    
    phi = 0;
    teta = 0;
}

void Camera::setScreenSize(int width, int height)
{
    scr_width = width;
    scr_height = height;
    scr_width_f = width;
    scr_height_f = height;
    half_scr_width_f_min_half_one = scr_width_f * 0.5 - 0.5;
    half_scr_height_f_min_half_one = scr_height_f * 0.5 - 0.5;
    
    screenRatio = scr_width_f / scr_height_f;
}
