//
//  Camera.hpp
//  ComputerGraphics
//
//  Created by Aleksandr Borodulin on 29/03/2019.
//  Copyright © 2019 Aleksandr Borodulin. All rights reserved.
//

#ifndef Camera_hpp
#define Camera_hpp

#include <memory>
#include "Math.h"
#include <cassert>

class Camera {
private:
    static Camera* currentCamera;

    int nearClip;
    int farClip;
    float clipSpace;
    float projDist;

    int scr_width;
    int scr_height;
    float scr_width_f;
    float scr_height_f;
    float half_scr_width_f_min_half_one;
    float half_scr_height_f_min_half_one;

    float screenRatio;
public:
    Vec3f pos;
    float phi;
    float teta;

    Camera() = delete;
    Camera(int nClip, int fClip, int screenWidth, int screenHeight);

    void setScreenSize(int width, int height);

    inline Mat4_4 getProjMatrix() { return axMatrix() * uvnMatrix() * transMatrix(); }

    inline void setProjDist(float p_dist) { assert(p_dist > 0); projDist = p_dist; }

    inline int screenWidth() { return scr_width; }
    inline int screenHeight() { return scr_height; }
    inline float screenWidth_f() { return scr_width_f; }
    inline float screenHeight_f() { return scr_height_f; }
    inline float half_screenWidth_f_mho() { return half_scr_width_f_min_half_one; }
    inline float half_screenHeight_f_mho() { return half_scr_height_f_min_half_one; }

    inline int getNearClip() { return nearClip; }
    inline int getFarClip() { return farClip; }

    inline Mat4_4 transMatrix();
    inline Mat4_4 uvnMatrix();
    inline Mat4_4 axMatrix();
};

Mat4_4 Camera::transMatrix() {
    return Mat4_4::translate(-pos.x, -pos.y, -pos.z);
}

Mat4_4 Camera::uvnMatrix() {


    Vec3f u;
    u.x = cos(teta) * sin(phi);
    u.y = sin(teta);
    u.z = cos(phi) * cos(teta);
    u = u.normalize();

    Vec3f n = Vec3f(0, 1, 0)^u;
    n = n.normalize();

    Vec3f v = u^n;
    v = v.normalize();

    Mat4_4 uvnMatrix = Mat4_4::identity();
    uvnMatrix[0][0] = n.x;
    uvnMatrix[0][1] = n.y;
    uvnMatrix[0][2] = n.z;

    uvnMatrix[1][0] = v.x;
    uvnMatrix[1][1] = v.y;
    uvnMatrix[1][2] = v.z;

    uvnMatrix[2][0] = u.x;
    uvnMatrix[2][1] = u.y;
    uvnMatrix[2][2] = u.z;

    return uvnMatrix;
}

Mat4_4 Camera::axMatrix() {
    Mat4_4 axProj = Mat4_4::identity();
    axProj[1][1] = screenRatio;
    axProj[3][2] = 1.f / projDist;
    axProj[3][3] = 0.f;
    
    float a = (farClip + nearClip) / clipSpace;
    float b = -2 * farClip * nearClip / clipSpace;
    
    axProj[2][2] = a;
    axProj[2][3] = b;
    
    return axProj;
}

#endif /* Camera_hpp */
