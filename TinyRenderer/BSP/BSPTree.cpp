//
//  BSPProcessor.cpp
//  ComputerGraphics
//
//  Created by Aleksandr Borodulin on 06/06/2019.
//  Copyright © 2019 Aleksandr Borodulin. All rights reserved.
//

#include "BSPTree.h"
#include <limits>
#include <stdlib.h>

#define HALF_PIXEL 0.5f

BSPTree::BSPTree(Model<double> *obj): _root(nullptr) {
    std::vector<Triangle<double>> faces;
    
    for (auto it = obj->faceBegin(); it != obj->faceEnd(); ++it) {
        
        Triangle<double> tr;
        
        for (int j = 0; j < 3; j++) {
            Vertex<double> vert;
            FVertex fVert = (*it)[j];
            
            vert.position = obj->vert(fVert.vertIdx);
            vert.texCoord = obj->texcoord(fVert.texIdx);
            
            tr[j] = vert;
        }
        
        Vec3df n = (tr.v1.position - tr.v0.position)^(tr.v2.position - tr.v0.position);
        n.normalize();
        
        tr.norm = n;
        
        faces.push_back(tr);
    }
    idCounter = 0;
    
    objMat = obj->objMat;
    texture = obj->getTexture();
    
    _root = split(faces);
}

BSPNode * BSPTree::split(arr<Triangle<double>> faces) {
    
    BSPNode* root = nullptr;
    
    arr<Triangle<double>> frontFaces;
    arr<Triangle<double>> backFaces;
    arr<Triangle<double>> complFaces;
    
    Vec4df p0;
    
    int rootFaceIndex = 0;
    
    int weights = std::numeric_limits<int>::max();
    int rootIndexAppl = 0;
    
    bool finishSplitting = false;
    for (auto it = faces.begin(); it != faces.end(); ++it) {
        
        splitting:
        
        delete root;
        root = new BSPNode();
        root->face = faces[rootFaceIndex];
        
        p0 = root->face.v0.position;
        
        if ((std::fabs(root->face.norm.x) + std::fabs(root->face.norm.y) + std::fabs(root->face.norm.z)) > EPS_VALUE) {
            int counter = -1;
            
            int fracTrianglesCount = 0;
            
            for (auto it = faces.begin(); it != faces.end(); ++it) {
                
                counter++;
                
                if (counter != rootFaceIndex) {
                    
                    Vec4df pt0 = (*it).v0.position;
                    Vec4df pt1 = (*it).v1.position;
                    Vec4df pt2 = (*it).v2.position;
                    
                    double hs[3];
                    
                    hs[0] = root->face.norm.x*(pt0.x - p0.x) + root->face.norm.y*(pt0.y - p0.y) + root->face.norm.z*(pt0.z - p0.z);
                    hs[1] = root->face.norm.x*(pt1.x - p0.x) + root->face.norm.y*(pt1.y - p0.y) + root->face.norm.z*(pt1.z - p0.z);
                    hs[2] = root->face.norm.x*(pt2.x - p0.x) + root->face.norm.y*(pt2.y - p0.y) + root->face.norm.z*(pt2.z - p0.z);
                    
                    if (std::fabs(hs[0]) < EPS_VALUE && std::fabs(hs[1]) < EPS_VALUE && std::fabs(hs[2]) < EPS_VALUE) {//complanar
                        complFaces.push_back(*it);
                    } else if (hs[0] >= -EPS_VALUE && hs[1] >= -EPS_VALUE && hs[2] >= -EPS_VALUE) {//front
                        frontFaces.push_back(*it);
                    } else if (hs[0] < -EPS_VALUE && hs[1] < -EPS_VALUE && hs[2] < -EPS_VALUE) {//back
                        backFaces.push_back(*it);
                    } else {
                        fracTrianglesCount++;
                        Vertex<double> frontSidePoints[2];
                        Vertex<double> backSidePoints[2];
                        
                        int frontCount = 0;
                        int backCount = 0;
                        
                        bool clockwise = true;
                        
                        for (int i = 0; i < 3; i++) {
                            if (hs[i] >= -EPS_VALUE) {
                                
                                frontSidePoints[frontCount] = (*(it))[i];
                                frontCount += 1;
                                
                                if (i == 1 && backCount == 1 && hs[2] < -EPS_VALUE) {
                                    clockwise = false;
                                }
                            } else {
                                backSidePoints[backCount] = (*(it))[i];
                                backCount += 1;
                                
                                if (i == 1 && frontCount == 1 && hs[2] >= -EPS_VALUE) {
                                    clockwise = false;
                                }
                            }
                        }
                        
                        if (frontCount == 1) {
                            
                            Triangle<double> frontFace;
                            
                            Vec4df v1 = backSidePoints[0].position - frontSidePoints[0].position;
                            Vec4df v2 = backSidePoints[1].position - frontSidePoints[0].position;
                            
                            double t1Koef = -(root->face.norm.x * v1.x + root->face.norm.y * v1.y + root->face.norm.z * v1.z);
                            double t2Koef = -(root->face.norm.x * v2.x + root->face.norm.y * v2.y + root->face.norm.z * v2.z);
                            
                            if (!(std::fabs(t1Koef) < EPS_VALUE || std::fabs(t2Koef) < EPS_VALUE)) {
                                
                                double t1 = (root->face.norm.x * frontSidePoints[0].position.x - root->face.norm.x*p0.x + root->face.norm.y * frontSidePoints[0].position.y - root->face.norm.y*p0.y + root->face.norm.z * frontSidePoints[0].position.z- root->face.norm.z*p0.z) / t1Koef;
                                
                                double t2 = (root->face.norm.x * frontSidePoints[0].position.x - root->face.norm.x*p0.x + root->face.norm.y * frontSidePoints[0].position.y - root->face.norm.y*p0.y + root->face.norm.z * frontSidePoints[0].position.z - root->face.norm.z*p0.z) / t2Koef;
                                
                                Vertex<double> clipVertex1;
                                
                                clipVertex1.position = frontSidePoints[0].position + v1 * t1;
                                clipVertex1.texCoord = frontSidePoints[0].texCoord + (backSidePoints[0].texCoord - frontSidePoints[0].texCoord) * t1;
                                
                                Vertex<double> clipVertex2;
                                
                                clipVertex2.position = frontSidePoints[0].position + v2 * t2;
                                clipVertex2.texCoord = frontSidePoints[0].texCoord + (backSidePoints[1].texCoord - frontSidePoints[0].texCoord) * t2;
                                
                                double l1 = sideLength(clipVertex2, clipVertex1);
                                double l2 = sideLength(clipVertex2, frontSidePoints[0]);
                                double l3 = sideLength(clipVertex1, frontSidePoints[0]);
                                
                                double l2_back = sideLength(clipVertex2, backSidePoints[1]);
                                
                                double D = -((root->face.norm.x * p0.x) + (root->face.norm.y * p0.y) + (root->face.norm.z * p0.z));
                                
                                double d1_front = fabs(root->face.norm.x * frontSidePoints[0].position.x + root->face.norm.y * frontSidePoints[0].position.y + root->face.norm.z * frontSidePoints[0].position.z + D) / sqrt(root->face.norm.x * root->face.norm.x + root->face.norm.y * root->face.norm.y + root->face.norm.z * root->face.norm.z);
                                
                                double d1_back = fabs(root->face.norm.x * backSidePoints[0].position.x + root->face.norm.y * backSidePoints[0].position.y + root->face.norm.z * backSidePoints[0].position.z + D) / sqrt(root->face.norm.x * root->face.norm.x + root->face.norm.y * root->face.norm.y + root->face.norm.z * root->face.norm.z);
                                
                                double d2_back = fabs(root->face.norm.x * backSidePoints[1].position.x + root->face.norm.y * backSidePoints[1].position.y + root->face.norm.z * backSidePoints[1].position.z + D) / sqrt(root->face.norm.x * root->face.norm.x + root->face.norm.y * root->face.norm.y + root->face.norm.z * root->face.norm.z);
                            
                                hs[0] = root->face.norm.x*(pt0.x - p0.x) + root->face.norm.y*(pt0.y - p0.y) + root->face.norm.z*(pt0.z - p0.z);
                                
                                if (d1_front < HALF_PIXEL) {
                                    backFaces.push_back(*it);
                                } else if (d1_back < HALF_PIXEL && d2_back < HALF_PIXEL) {
                                    frontFaces.push_back(*it);
                                } else if (l2_back < HALF_PIXEL) {
                                    //Back faces
                                    l1 = sideLength(backSidePoints[1], clipVertex1);
                                    l2 = sideLength(backSidePoints[1], clipVertex2);
                                    l3 = sideLength(clipVertex1, clipVertex2);
                                    
                                    if (l1 < HALF_PIXEL || l2 < HALF_PIXEL) {
                                        if (clockwise) {
                                            frontFace.v0 = clipVertex1;
                                            frontFace.v1 = backSidePoints[1];
                                            frontFace.v2 = frontSidePoints[0];
                                        } else {
                                            frontFace.v0 = frontSidePoints[0];
                                            frontFace.v1 = backSidePoints[1];
                                            frontFace.v2 = clipVertex1;
                                        }
                                        frontFace.norm = it->norm;
                                        frontFaces.push_back(frontFace);
                                    } else {
                                        if (clockwise) {
                                            frontFace.v0 = frontSidePoints[0];
                                            frontFace.v1 = clipVertex1;
                                            frontFace.v2 = clipVertex2;
                                        } else {
                                            frontFace.v0 = clipVertex2;
                                            frontFace.v1 = clipVertex1;
                                            frontFace.v2 = frontSidePoints[0];
                                        }
                                        frontFace.norm = it->norm;
                                        frontFaces.push_back(frontFace);
                                        
                                        Triangle<double> backFace1;
                                        if (clockwise) {
                                            backFace1.v0 = clipVertex2;
                                            backFace1.v1 = clipVertex1;
                                            backFace1.v2 = backSidePoints[1];
                                        } else {
                                            backFace1.v0 = backSidePoints[1];
                                            backFace1.v1 = clipVertex1;
                                            backFace1.v2 = clipVertex2;
                                        }
                                        backFace1.norm = it->norm;
                                        backFaces.push_back(backFace1);
                                    }
                                    
                                    //In process
                                    
                                    l1 = sideLength(clipVertex1, backSidePoints[1]);
                                    l2 = sideLength(clipVertex1, backSidePoints[0]);
                                    l3 = sideLength(backSidePoints[1], backSidePoints[0]);
                                    
                                    if (l1 < HALF_PIXEL || l2 < HALF_PIXEL || l3 < HALF_PIXEL) {
                                        //fracTrianglesCount += 1;
                                    }
                                    
                                    Triangle<double> backFace2;
                                    if (clockwise) {
                                        backFace2.v0 = backSidePoints[0];
                                        backFace2.v1 = backSidePoints[1];
                                        backFace2.v2 = clipVertex1;
                                    } else {
                                        backFace2.v0 = clipVertex1;
                                        backFace2.v1 = backSidePoints[1];
                                        backFace2.v2 = backSidePoints[0];
                                    }
                                    backFace2.norm = it->norm;
                                    backFaces.push_back(backFace2);
                                } else {
                                    //Back faces
                                    l1 = sideLength(backSidePoints[0], clipVertex1);
                                    l2 = sideLength(backSidePoints[0], clipVertex2);
                                    l3 = sideLength(clipVertex1, clipVertex2);
                                    
                                    if (l1 < HALF_PIXEL || l2 < HALF_PIXEL) {
                                        if (clockwise) {
                                            frontFace.v0 = clipVertex2;
                                            frontFace.v1 = frontSidePoints[0];
                                            frontFace.v2 = backSidePoints[0];
                                        } else {
                                            frontFace.v0 = backSidePoints[0];
                                            frontFace.v1 = frontSidePoints[0];
                                            frontFace.v2 = clipVertex2;
                                        }
                                        frontFace.norm = it->norm;
                                        frontFaces.push_back(frontFace);
                                    } else {
                                        if (clockwise) {
                                            frontFace.v0 = frontSidePoints[0];
                                            frontFace.v1 = clipVertex1;
                                            frontFace.v2 = clipVertex2;
                                        } else {
                                            frontFace.v0 = clipVertex2;
                                            frontFace.v1 = clipVertex1;
                                            frontFace.v2 = frontSidePoints[0];
                                        }
                                        frontFace.norm = it->norm;
                                        frontFaces.push_back(frontFace);
                                        
                                        Triangle<double> backFace1;
                                        if (clockwise) {
                                            backFace1.v0 = clipVertex2;
                                            backFace1.v1 = clipVertex1;
                                            backFace1.v2 = backSidePoints[0];
                                        } else {
                                            backFace1.v0 = backSidePoints[0];
                                            backFace1.v1 = clipVertex1;
                                            backFace1.v2 = clipVertex2;
                                        }
                                        backFace1.norm = it->norm;
                                        backFaces.push_back(backFace1);
                                    }
                                    
                                    l1 = sideLength(clipVertex2, backSidePoints[1]);
                                    l2 = sideLength(clipVertex2, backSidePoints[0]);
                                    l3 = sideLength(backSidePoints[1], backSidePoints[0]);
                                    
                                    if (l1 < HALF_PIXEL || l2 < HALF_PIXEL || l3 < HALF_PIXEL) {
                                        //fracTrianglesCount += 1;
                                    }
                                    
                                    Triangle<double> backFace2;
                                    if (clockwise) {
                                        backFace2.v0 = backSidePoints[0];
                                        backFace2.v1 = backSidePoints[1];
                                        backFace2.v2 = clipVertex2;
                                    } else {
                                        backFace2.v0 = clipVertex2;
                                        backFace2.v1 = backSidePoints[1];
                                        backFace2.v2 = backSidePoints[0];
                                    }
                                    backFace2.norm = it->norm;
                                    backFaces.push_back(backFace2);
                                }
                            } else {
                                backFaces.push_back(*it);
                            }
                        } else {
                            Triangle<double> backFace;
                            
                            Vec4df v1 = frontSidePoints[0].position - backSidePoints[0].position;
                            Vec4df v2 = frontSidePoints[1].position - backSidePoints[0].position;
                            
                            double t1Koef = -(root->face.norm.x * v1.x + root->face.norm.y * v1.y + root->face.norm.z * v1.z);
                            double t2Koef = -(root->face.norm.x * v2.x + root->face.norm.y * v2.y + root->face.norm.z * v2.z);
                            
                            if (!(std::fabs(t1Koef) < EPS_VALUE || std::fabs(t2Koef) < EPS_VALUE)) {
                                
                                double t1 = (root->face.norm.x * backSidePoints[0].position.x - root->face.norm.x*p0.x + root->face.norm.y * backSidePoints[0].position.y - root->face.norm.y*p0.y + root->face.norm.z * backSidePoints[0].position.z- root->face.norm.z*p0.z) / t1Koef;
                                
                                double t2 = (root->face.norm.x * backSidePoints[0].position.x - root->face.norm.x*p0.x + root->face.norm.y * backSidePoints[0].position.y - root->face.norm.y*p0.y + root->face.norm.z * backSidePoints[0].position.z - root->face.norm.z*p0.z) / t2Koef;
                                
                                Vertex<double> clipVertex1;
                                
                                clipVertex1.position = backSidePoints[0].position + v1 * t1;
                                clipVertex1.texCoord = backSidePoints[0].texCoord + (frontSidePoints[0].texCoord - backSidePoints[0].texCoord) * t1;
                                
                                Vertex<double> clipVertex2;
                                
                                clipVertex2.position = backSidePoints[0].position + v2 * t2;
                                clipVertex2.texCoord = backSidePoints[0].texCoord + (frontSidePoints[1].texCoord - backSidePoints[0].texCoord) * t2;
                                
                                
                                double l1 = sideLength(backSidePoints[0], clipVertex1);
                                double l2 = sideLength(backSidePoints[0], clipVertex2);
                                double l3 = sideLength(clipVertex1, clipVertex2);
                                
                                double l2_front = sideLength(clipVertex2, frontSidePoints[1]);
                                
                                double D = -((root->face.norm.x * p0.x) + (root->face.norm.y * p0.y) + (root->face.norm.z * p0.z));
                                
                                double d1_back = fabs(root->face.norm.x * backSidePoints[0].position.x + root->face.norm.y * backSidePoints[0].position.y + root->face.norm.z * backSidePoints[0].position.z + D) / sqrt(root->face.norm.x * root->face.norm.x + root->face.norm.y * root->face.norm.y + root->face.norm.z * root->face.norm.z);
                                
                                double d1_front = fabs(root->face.norm.x * frontSidePoints[0].position.x + root->face.norm.y * frontSidePoints[0].position.y + root->face.norm.z * frontSidePoints[0].position.z + D) / sqrt(root->face.norm.x * root->face.norm.x + root->face.norm.y * root->face.norm.y + root->face.norm.z * root->face.norm.z);
                                
                                double d2_front = fabs(root->face.norm.x * frontSidePoints[1].position.x + root->face.norm.y * frontSidePoints[1].position.y + root->face.norm.z * frontSidePoints[1].position.z + D) / sqrt(root->face.norm.x * root->face.norm.x + root->face.norm.y * root->face.norm.y + root->face.norm.z * root->face.norm.z);
                                
                                if (d1_back < HALF_PIXEL) {
                                    frontFaces.push_back(*it);
                                } else if (d1_front < HALF_PIXEL && d2_front < HALF_PIXEL) {
                                    backFaces.push_back(*it);
                                } else if (l2_front < HALF_PIXEL) {
                                    //Front faces
                                    
                                    l1 = sideLength(clipVertex2, clipVertex1);
                                    l2 = sideLength(clipVertex2, frontSidePoints[1]);
                                    l3 = sideLength(clipVertex1, frontSidePoints[1]);
                                    
                                    if (l2 < HALF_PIXEL || l3 < HALF_PIXEL) {
                                        if (clockwise) {
                                            backFace.v0 = clipVertex1;
                                            backFace.v1 = frontSidePoints[1];
                                            backFace.v2 = backSidePoints[0];
                                        } else {
                                            backFace.v0 = backSidePoints[0];
                                            backFace.v1 = frontSidePoints[1];
                                            backFace.v2 = clipVertex1;
                                        }
                                        backFace.norm = it->norm;
                                        backFaces.push_back(backFace);
                                    } else {
                                        
                                        if (clockwise) {
                                            backFace.v0 = backSidePoints[0];
                                            backFace.v1 = clipVertex1;
                                            backFace.v2 = clipVertex2;
                                        } else {
                                            backFace.v0 = clipVertex2;
                                            backFace.v1 = clipVertex1;
                                            backFace.v2 = backSidePoints[0];
                                        }
                                        backFace.norm = it->norm;
                                        backFaces.push_back(backFace);
                                        
                                        Triangle<double> frontFace1;
                                        if (clockwise) {
                                            frontFace1.v0 = clipVertex2;
                                            frontFace1.v1 = clipVertex1;
                                            frontFace1.v2 = frontSidePoints[1];
                                        } else {
                                            frontFace1.v0 = frontSidePoints[1];
                                            frontFace1.v1 = clipVertex1;
                                            frontFace1.v2 = clipVertex2;
                                        }
                                        frontFace1.norm = it->norm;
                                        frontFaces.push_back(frontFace1);
                                    }
                                    
                                    //In process
                                    
                                    l1 = sideLength(frontSidePoints[0], frontSidePoints[1]);
                                    l2 = sideLength(frontSidePoints[0], clipVertex1);
                                    l3 = sideLength(frontSidePoints[1], clipVertex1);
                                    
                                    if (l1 < HALF_PIXEL || l2 < HALF_PIXEL || l3 < HALF_PIXEL) {
                                        //fracTrianglesCount += 1;
                                    }
                                    
                                    Triangle<double> frontFace2;
                                    if (clockwise) {
                                        frontFace2.v0 = frontSidePoints[0];
                                        frontFace2.v1 = frontSidePoints[1];
                                        frontFace2.v2 = clipVertex1;
                                    } else {
                                        frontFace2.v0 = clipVertex1;
                                        frontFace2.v1 = frontSidePoints[1];
                                        frontFace2.v2 = frontSidePoints[0];
                                    }
                                    frontFace2.norm = it->norm;
                                    frontFaces.push_back(frontFace2);
                                } else {
                                    //Front faces
                                    l1 = sideLength(clipVertex2, clipVertex1);
                                    l2 = sideLength(clipVertex2, frontSidePoints[0]);
                                    l3 = sideLength(clipVertex1, frontSidePoints[0]);
                                    
                                    if (l2 < HALF_PIXEL || l3 < HALF_PIXEL) {
                                        if (clockwise) {
                                            backFace.v0 = clipVertex2;
                                            backFace.v1 = backSidePoints[0];
                                            backFace.v2 = frontSidePoints[0];
                                        } else {
                                            backFace.v0 = frontSidePoints[0];
                                            backFace.v1 = backSidePoints[0];
                                            backFace.v2 = clipVertex2;
                                        }
                                        backFace.norm = it->norm;
                                        backFaces.push_back(backFace);
                                    } else {
                                        if (clockwise) {
                                            backFace.v0 = backSidePoints[0];
                                            backFace.v1 = clipVertex1;
                                            backFace.v2 = clipVertex2;
                                        } else {
                                            backFace.v0 = clipVertex2;
                                            backFace.v1 = clipVertex1;
                                            backFace.v2 = backSidePoints[0];
                                        }
                                        backFace.norm = it->norm;
                                        backFaces.push_back(backFace);
                                        
                                        Triangle<double> frontFace1;
                                        if (clockwise) {
                                            frontFace1.v0 = clipVertex2;
                                            frontFace1.v1 = clipVertex1;
                                            frontFace1.v2 = frontSidePoints[0];
                                        } else {
                                            frontFace1.v0 = frontSidePoints[0];
                                            frontFace1.v1 = clipVertex1;
                                            frontFace1.v2 = clipVertex2;
                                        }
                                        frontFace1.norm = it->norm;
                                        frontFaces.push_back(frontFace1);
                                    }
                                    
                                    l1 = sideLength(frontSidePoints[0], frontSidePoints[1]);
                                    l2 = sideLength(frontSidePoints[0], clipVertex2);
                                    l3 = sideLength(frontSidePoints[1], clipVertex2);
                                    
                                    if (l1 < HALF_PIXEL || l2 < HALF_PIXEL || l3 < HALF_PIXEL) {
                                        //fracTrianglesCount += 1;
                                    }
                                    
                                    Triangle<double> frontFace2;
                                    if (clockwise) {
                                        frontFace2.v0 = frontSidePoints[0];
                                        frontFace2.v1 = frontSidePoints[1];
                                        frontFace2.v2 = clipVertex2;
                                    } else {
                                        frontFace2.v0 = clipVertex2;
                                        frontFace2.v1 = frontSidePoints[1];
                                        frontFace2.v2 = frontSidePoints[0];
                                    }
                                    frontFace2.norm = it->norm;
                                    frontFaces.push_back(frontFace2);
                                }
                            } else {
                                std::cout << (*it).identifier;
                                frontFaces.push_back(*it);
                            }
                        }
                    }
                }
            }
            
            if (weights > /*(frontFaces.size() + backFaces.size()) + */fracTrianglesCount) {
                weights = /*frontFaces.size() + backFaces.size() + */fracTrianglesCount;
                rootIndexAppl = rootFaceIndex;
            }
        } else {
            std::cout << "Fail";
        }
        
        
        if (finishSplitting) {
            if (weights > 0) {
                std::cout << "Fail";
            }
            break;
        }
        
        frontFaces.clear();
        backFaces.clear();
        complFaces.clear();
        
        if ((it + 1) == faces.end()) {
            finishSplitting = true;
            rootFaceIndex = rootIndexAppl;
            if (weights > 0) {
                //std::cout << weights << std::endl;
            }
            goto splitting;
        }
        rootFaceIndex++;
    }
    if (!frontFaces.empty()) {
        root->front = split(frontFaces);
        if (root->front->face.identifier == 3) {
            std::cout << "Success";
        }
    }
    
    if (!backFaces.empty()) {
        root->back = split(backFaces);
        if (root->back->face.identifier == 3) {
            std::cout << "Success";
        }
    }
    
    if (!complFaces.empty()) {
        root->complanarFaces.clear();
        for (auto it = complFaces.begin(); it != complFaces.end(); it++) {
            Triangle<float> tr; tr = *it;
            root->complanarFaces.push_back(tr);
        }
    }
    
    if (root->face.identifier == 3) {
        std::cout << "Success";
    }
    return root;
}

double BSPTree::sideLength(Vertex<double> v1, Vertex<double> v2) {
    return sqrt((v2.position.x - v1.position.x) * (v2.position.x - v1.position.x) + (v2.position.y - v1.position.y) * (v2.position.y - v1.position.y) + (v2.position.z - v1.position.z) * (v2.position.z - v1.position.z));
}
