//
//  BSPProcessor.h
//  ComputerGraphics
//
//  Created by Aleksandr Borodulin on 06/06/2019.
//  Copyright © 2019 Aleksandr Borodulin. All rights reserved.
//

#ifndef BSPTree_h
#define BSPTree_h

#include "BSPNode.h"
#include "Texture.hpp"
#include "model.h"

class BSPTree {
private:
    BSPNode* _root;
    
    BSPNode * split(arr<Triangle<double>> faces);
    
    double sideLength(Vertex<double> v1, Vertex<double> v2);
    
    int idCounter;
    
    texture_shared_ptr texture;
public:
    Mat4_4 objMat;
    
    BSPTree(Model<double> *obj);
    
    ~BSPTree() { delete _root; }
    
    inline BSPNode* getRoot() { return _root; }
    
    inline texture_shared_ptr getTexture() { return texture; }
};

#endif /* BSPProcessor_h */
