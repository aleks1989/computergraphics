//
//  BSPNode.h
//  ComputerGraphics
//
//  Created by Aleksandr Borodulin on 06/06/2019.
//  Copyright © 2019 Aleksandr Borodulin. All rights reserved.
//

#ifndef BSPNode_h
#define BSPNode_h

#include "Vertex.h"

struct BSPNode {
    Triangle<float> face;
    
    BSPNode* front = nullptr;
    BSPNode* back = nullptr;
    
    arr<Triangle<float>> complanarFaces;
    
    ~BSPNode() { delete front; delete back; }
};

#endif /* BSPNode_h */
