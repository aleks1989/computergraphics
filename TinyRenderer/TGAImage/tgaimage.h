#ifndef __IMAGE_H__
#define __IMAGE_H__

#include <fstream>
#include <string.h>

#include "Color.h"

#pragma pack(push,1)
struct TGA_Header {
    char idlength;
    char colormaptype;
    char datatypecode;
    short colormaporigin;
    short colormaplength;
    char colormapdepth;
    short x_origin;
    short y_origin;
    short width;
    short height;
    char  bitsperpixel;
    char  imagedescriptor;
};
#pragma pack(pop)

class TGAImage {
protected:
	unsigned char* data;
	int width;
	int height;
	int bytespp;

	bool   load_rle_data(std::ifstream &in);
	bool unload_rle_data(std::ofstream &out);
public:
	TGAImage();
	TGAImage(int w, int h, int bpp);
	TGAImage(const TGAImage &img);
    bool read_tga_file(const char* filename);
	bool write_tga_file(const char* filename, bool rle=true);
	bool flip_horizontally();
	bool flip_vertically();
	bool scale(int w, int h);
    inline RGBA get(int x, int y);
    bool set(int x, int y, RGBA c);
	~TGAImage();
    TGAImage & operator =(const TGAImage &img);
	int get_width();
	int get_height();
	int get_bytespp();
	unsigned char *buffer();
	void clear();
};

RGBA TGAImage::get(int x, int y) {
    if (x<0 || y<0 || x>=width || y>=height) {
        return RGBA(0xFF0000FF);
    }
    uchar* bgr = data+(x+y*width)*bytespp;
    return RGBA(bgr[2], bgr[1], bgr[0], 255);
}

#endif //__IMAGE_H__
