//
//  Texture.hpp
//  ComputerGraphics
//
//  Created by Aleksandr Borodulin on 16/04/2019.
//  Copyright © 2019 Aleksandr Borodulin. All rights reserved.
//

#ifndef Texture_hpp
#define Texture_hpp

#include "tgaimage.h"
#include <memory>

class Texture;

typedef std::shared_ptr<Texture> texture_shared_ptr;

class Texture {
private:
    TGAImage* img;

    Texture(const char* path);
public:
    Texture() = delete;

    ~Texture();

    inline TGAImage* tga_image() { return img; }

    static texture_shared_ptr CreateTexture(const char* path);
};

#endif /* Texture_hpp */
