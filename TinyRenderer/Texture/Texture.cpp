//
//  Texture.cpp
//  ComputerGraphics
//
//  Created by Aleksandr Borodulin on 16/04/2019.
//  Copyright © 2019 Aleksandr Borodulin. All rights reserved.
//

#include "Texture.hpp"

Texture::Texture(const char* path) {
    img = new TGAImage();
    img->read_tga_file(path);
    img->flip_vertically();
}

Texture::~Texture() {
    delete img;
}

texture_shared_ptr Texture::CreateTexture(const char* path)
{
    return texture_shared_ptr(new Texture(path));
}
