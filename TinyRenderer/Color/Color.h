//
//  Color.hpp
//  ComputerGraphics
//
//  Created by Aleksandr Borodulin on 18/04/2019.
//  Copyright © 2019 Aleksandr Borodulin. All rights reserved.
//

#ifndef Color_h
#define Color_h

typedef unsigned char uchar;

typedef struct Color {
    union {
        struct {uchar r, g, b, a;};
        uchar raw[4];
        int value;
    };
    
    Color() {}
    Color(uchar red, uchar green, uchar blue, uchar alpha) : r(red), g(green), b(blue), a(alpha) {}
    Color(int val) : value(val) {}
} RGBA;

#endif /* Color_hpp */
