#ifndef __MODEL_H__
#define __MODEL_H__

#include "Array.h"
#include "Math.h"
#include "Texture.hpp"
#include <fstream>
#include <sstream>

struct FVertex
{
    int vertIdx;
    int texIdx;
    FVertex() {}
    FVertex(int vIndex, int tIndex) : vertIdx(vIndex), texIdx(tIndex) {}
};

struct Face
{
private:
    static int idCounter;
    arr<FVertex> verts;
public:
    int identifier;
    Face();
    Face(FVertex v1, FVertex v2, FVertex v3);
    
    FVertex& operator[](const int i) { assert(i < 3); return verts[i]; }
};

template <typename T>
class Model
{
private:
	std::vector<Vec3<T>> verts_;
    std::vector<Vec2f> texcoords_;
	std::vector<Face> faces_;
    
    texture_shared_ptr texture;
    
    float radius;
public:
    Mat4_4 objMat;
    Model() = delete;
    Model(const char *filename, Vec3<T> scale = Vec3<T>(1.0, 1.0, 1.0), texture_shared_ptr textureItem = nullptr);
    Model(std::vector<Vec3<T>> verts,
          std::vector<Vec2f> texcoords,
          std::vector<Face> faces,
          texture_shared_ptr textureItem);
	unsigned long nverts();
	unsigned long nfaces();
	Vec3<T> vert(int i);
    Vec2f texcoord(int i);
	std::vector<FVertex> face(int idx);
    
    inline texture_shared_ptr getTexture() { return texture; }
    
    auto faceBegin() { return faces_.begin(); }
    auto faceEnd() { return faces_.end(); }
};

template <class T>
Model<T>::Model(const char *filename, Vec3<T> scale, texture_shared_ptr textureItem) : verts_(), faces_(), radius(0.f), texture(textureItem) {
    
    float texWidth = textureItem->tga_image()->get_width();
    float texHeight = textureItem->tga_image()->get_height();
    
    objMat = Mat4_4::identity();
    std::ifstream in;
    in.open (filename, std::ifstream::in);
    if (in.fail()) return;
    std::string line;
    std::string strash;
    while (!in.eof()) {
        std::getline(in, line);
        std::istringstream iss(line.c_str());
        char trash;
        if (!line.compare(0, 2, "v ")) {
            iss >> trash;
            Vec3<T> v;
            T raw;
            for (int i = 0; i < 3; i++) {
                if (iss >> raw) {
                    switch (i) {
                        case 0:
                            v.raw[i] = raw * scale.x;
                            break;
                        case 1:
                            v.raw[i] = raw * scale.y;
                            break;
                        case 2:
                            v.raw[i] = -1 * raw * scale.z; //Convert to left hand coordinate system
                            break;
                        default:
                            break;
                    }
                }
            }
            
            if ((v.x*v.x + v.y*v.y + v.z*v.z) > radius) {
                radius = (v.x*v.x + v.y*v.y + v.z*v.z);
            }
            
            verts_.push_back(v);
        } else if (!line.compare(0, 3, "vt ")) {
            iss >> trash >> trash;
            Vec2f vt;
            float raw;
            for (int i = 0; i < 3; i++) {
                if (iss >> raw) {
                    switch (i) {
                        case 0:
                            vt.raw[i] = raw * texWidth;
                            break;
                        case 1:
                            vt.raw[i] = raw * texHeight;
                            break;
                        default:
                            break;
                    }
                }
            }
            texcoords_.push_back(vt);
        } else if (!line.compare(0, 2, "f ")) {
            
            int ivalue;
            iss >> trash;
            
            arr<FVertex> verts;
            
            while (iss >> ivalue >> strash) {
                FVertex fVert;
                fVert.vertIdx = --ivalue;
                std::istringstream iss(strash.c_str());
                if (iss >> trash >> ivalue >> strash) {
                    fVert.texIdx = --ivalue;
                }
                verts.push_back(fVert);
            }
            
            if (verts.size() == 4) {
                Face tf1(verts[0], verts[1], verts[2]);
                Face tf2(verts[0], verts[2], verts[3]);
                faces_.push_back(tf1);
                faces_.push_back(tf2);
            } else {
                Face f(verts[0], verts[1], verts[2]);
                faces_.push_back(f);
            }
            
        }
    }
    
    radius = sqrt(radius);
    std::cout << "# v# " << verts_.size() << " f# "  << faces_.size() << " vt " << texcoords_.size() << std::endl;
}

template <class T>
Model<T>::Model(std::vector<Vec3<T>> verts,
                std::vector<Vec2f> texcoords,
                std::vector<Face> faces,
                texture_shared_ptr textureItem) : verts_(verts), texcoords_(texcoords), faces_(faces), texture(textureItem) {
    
    objMat = Mat4_4::identity();
    
    float texWidth = textureItem->tga_image()->get_width();
    float texHeight = textureItem->tga_image()->get_height();
    
    for (auto it = texcoords_.begin() ; it != texcoords_.end(); ++it) {
        it->x = it->x * texWidth;
        it->y = it->y * texHeight;
    }
}

template <class T>
unsigned long Model<T>::nverts() {
    return verts_.size();
}

template <class T>
unsigned long Model<T>::nfaces() {
    return faces_.size();
}

template <class T>
std::vector<FVertex> Model<T>::face(int idx) {
    return faces_[idx];
}

template <class T>
Vec3<T> Model<T>::vert(int i) {
    return verts_[i];
}

template <class T>
Vec2f Model<T>::texcoord(int i) {
    return texcoords_[i];
}

#endif //__MODEL_H__
