#include "model.h"

int Face::idCounter;

Face::Face()
{
    Face::idCounter++;
    identifier = Face::idCounter;
    verts.resize(3);
}

Face::Face(FVertex v1, FVertex v2, FVertex v3)
{
    Face::idCounter++;
    identifier = Face::idCounter;
    verts.push_back(v1);
    verts.push_back(v2);
    verts.push_back(v3);
}
