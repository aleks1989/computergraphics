#if defined(UNICODE) && !defined(_UNICODE)
    #define _UNICODE
#elif defined(_UNICODE) && !defined(UNICODE)
    #define UNICODE
#endif

#include <tchar.h>
#include <windows.h>

#include <iostream>

#include <ctime>
#include <cmath>
#include <stdio.h>
#include <math.h>

#include "tgaimage.h"
#include "model.h"
#include "Render.h"
#include "Camera.hpp"

#define SCR_WIDTH 600
#define SCR_HEIGHT 600

RGBQUAD frameBuffer[600][600];

TCHAR szClassName[ ] = _T("CodeBlocksWindowsApp");

Model* headObj;
Model* floorObj;

Render* renderer;
Camera* currCamera;

bool needToUpdate = true;

float elapsedTime;

/*  Declare Windows procedure  */
LRESULT CALLBACK WindowProcedure (HWND, UINT, WPARAM, LPARAM);

void PresentFrame(unsigned int width, unsigned int height, void* pixels, HWND hWnd);
void RenderObjects();

void UpdateElapsedTime();
inline time_t GetTimestamp();

void Init()
{
std::vector<Vec3f> vertices = {
        Vec3f(-100.f, 100.f, 0.f), Vec3f(100.f, 100.f, 0.f), Vec3f(100.f, -100.f, 0.f), Vec3f(-100.f, -100.f, 0.f),
        Vec3f(-100.f, 0.f, 100.f), Vec3f(100.f, 0.f, 100.f), Vec3f(100.f, 0.f, -100.f), Vec3f(-100.f, 0.f, -100.f)
    };

    std::vector<Vec2f> texCoords = {
        Vec2f(0.f, 0.f), Vec2f(1.f, 0.f), Vec2f(1.f, 1.f), Vec2f(0.f, 1.f),
        Vec2f(0.f, 0.f), Vec2f(1.f, 0.f), Vec2f(1.f, 1.f), Vec2f(0.f, 1.f)
    };

    std::vector<FVertex> face1 = {
        FVertex(2, 2), FVertex(1, 1), FVertex(0, 0)
    };

    std::vector<FVertex> face2 = {
        FVertex(2, 2), FVertex(0, 0), FVertex(3, 3)
    };

    std::vector<FVertex> face3 = {
        FVertex(0 + 4, 0 + 4), FVertex(1 + 4, 1 + 4), FVertex(2 + 4, 2 + 4),
    };

    std::vector<FVertex> face4 = {
        FVertex(3 + 4, 3 + 4), FVertex(0 + 4, 0 + 4), FVertex(2 + 4, 2 + 4)
    };

    std::vector<std::vector<FVertex> > faces;

    faces.push_back(face1);
    faces.push_back(face2);
    faces.push_back(face3);
    faces.push_back(face4);


    floorObj = new Model(vertices, texCoords, faces, Texture::CreateTexture("floor_diffuse.tga"));

    floorObj->objMat = Mat4_4::translate(0.f, -100.f, 300) * Mat4_4::rotateX(1.5708);

    //floorObj->createBSP();

    currCamera = new Camera(1, 500, SCR_WIDTH, SCR_HEIGHT);

    UpdateElapsedTime();
}

int WINAPI WinMain (HINSTANCE hThisInstance,
                     HINSTANCE hPrevInstance,
                     LPSTR lpszArgument,
                     int nCmdShow)
{
    HWND hwnd;               /* This is the handle for our window */
    MSG messages;            /* Here messages to the application are saved */
    WNDCLASSEX wincl;        /* Data structure for the windowclass */

    /* The Window structure */
    wincl.hInstance = hThisInstance;
    wincl.lpszClassName = szClassName;
    wincl.lpfnWndProc = WindowProcedure;      /* This function is called by windows */
    wincl.style = CS_DBLCLKS;                 /* Catch double-clicks */
    wincl.cbSize = sizeof (WNDCLASSEX);

    /* Use default icon and mouse-pointer */
    wincl.hIcon = LoadIcon (NULL, IDI_APPLICATION);
    wincl.hIconSm = LoadIcon (NULL, IDI_APPLICATION);
    wincl.hCursor = LoadCursor (NULL, IDC_ARROW);
    wincl.lpszMenuName = NULL;                 /* No menu */
    wincl.cbClsExtra = 0;                      /* No extra bytes after the window class */
    wincl.cbWndExtra = 0;                      /* structure or the window instance */
    /* Use Windows's default colour as the background of the window */
    wincl.hbrBackground = (HBRUSH) COLOR_BACKGROUND;

    /* Register the window class, and if it fails quit the program */
    if (!RegisterClassEx (&wincl))
        return 0;

    /* The class is registered, let's create the program*/
    hwnd = CreateWindowEx (
           0,                   /* Extended possibilites for variation */
           szClassName,         /* Classname */
           _T("Code::Blocks Template Windows App"),       /* Title Text */
           WS_OVERLAPPEDWINDOW, /* default window */
           CW_USEDEFAULT,       /* Windows decides the position */
           CW_USEDEFAULT,       /* where the window ends up on the screen */
           SCR_WIDTH,                 /* The programs width */
           SCR_HEIGHT,                 /* and height in pixels */
           HWND_DESKTOP,        /* The window is a child-window to desktop */
           NULL,                /* No menu */
           hThisInstance,       /* Program Instance handler */
           NULL                 /* No Window Creation data */
           );

    /* Make the window visible on the screen */
    ShowWindow (hwnd, nCmdShow);

    for(int r = 0; r < SCR_WIDTH; r++)
    {
        for(int c = 0; c < SCR_HEIGHT; c++)
        {
            frameBuffer[r][c] = {0, 0, 0, 0};
        }
    }

    frameBuffer[100][100] = {0, 255, 0, 0};

    Init();

    /* Run the message loop. It will run until GetMessage() returns 0 */
    while (GetMessage (&messages, NULL, 0, 0))
    {
        /* Translate virtual-key messages into character messages */
        TranslateMessage(&messages);
        /* Send message to WindowProcedure */
        DispatchMessage(&messages);

        UpdateElapsedTime();

        if (needToUpdate)
        {
            RenderObjects();

            for(int i = 0; i < SCR_WIDTH; i++)
            {
                for(int j = 0; j < SCR_HEIGHT; j++)
                {
                    int idx = i * currCamera->screenWidth() + j;
                    int invIdx = (currCamera->screenHeight() - i - 1) * currCamera->screenWidth() + j;
                    RGBA color = renderer->renderBuffer[invIdx];
                    //pix[invIdx] = color.value;
                    frameBuffer[i][j] = {color.r, color.g, color.b, color.a};
                }
            }

            PresentFrame(SCR_WIDTH, SCR_HEIGHT, frameBuffer, hwnd);

            needToUpdate = false;
        }

    }

    /* The program return-value is 0 - The value that PostQuitMessage() gave */
    return messages.wParam;
}


/*  This function is called by the Windows function DispatchMessage()  */

LRESULT CALLBACK WindowProcedure (HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch (message)                  /* handle the messages */
    {
        case WM_DESTROY:
            PostQuitMessage (0);       /* send a WM_QUIT to the message queue */
            break;
        case WM_KEYDOWN:
            switch (wParam)
            {
            case 87: //w key
                currCamera->pos.z += elapsedTime * 0.05 * cos(currCamera->phi);
                currCamera->pos.x += elapsedTime * 0.05 * sin(currCamera->phi);
                needToUpdate = true;
                break;
            case 83: //s key
                currCamera->pos.z -= elapsedTime * 0.05 * cos(currCamera->phi);
                currCamera->pos.x -= elapsedTime * 0.05 * sin(currCamera->phi);
                needToUpdate = true;
                break;
            case 65: //a key
                currCamera->pos.z += elapsedTime * 0.05 * sin(currCamera->phi);
                currCamera->pos.x -= elapsedTime * 0.05 * cos(currCamera->phi);
                needToUpdate = true;
                break;
            case 68: //d key
                currCamera->pos.z -= elapsedTime * 0.05 * sin(currCamera->phi);
                currCamera->pos.x += elapsedTime * 0.05 * cos(currCamera->phi);
                needToUpdate = true;
                break;
            default:
                break;
            }
            //std::cout << wParam << std::endl;
            break;
        default:                      /* for messages that we don't deal with */
            return DefWindowProc (hwnd, message, wParam, lParam);
    }

    return 0;
}

void PresentFrame(unsigned int width, unsigned int height, void* pixels, HWND hWnd)
{
    HBITMAP hBitMap = CreateBitmap(width, height, 1, 8 * 4, pixels);

    HDC hdc = GetDC(hWnd);

    HDC srcHdc = CreateCompatibleDC(hdc);

    SelectObject(srcHdc, hBitMap);

    BitBlt(hdc, 0, 0, width, height, srcHdc, 0, 0, SRCCOPY);

    DeleteObject(hBitMap);

    DeleteDC(srcHdc);

    DeleteDC(hdc);
}

float floorZRotate = 0;

void RenderObjects()
{
    if (currCamera == nullptr) {
        return;
    }

    if (!renderer)
    {
        renderer = new Render(currCamera);
    }

    //currCamera->phi -= 0.005;

    renderer->cleanRenderBuffer();
    renderer->cleanZBuffer();

    //floorZRotate -= 0.001;
    floorObj->objMat = Mat4_4::rotateX(floorZRotate) * Mat4_4::translate(0.f, -100.f, 300) * Mat4_4::rotateX(1.5708);
    //headObj->objMat = Mat4_4::rotateX(floorZRotate) * Mat4_4::translate(0.f, 0.f, 200);

    //currCamera->pos.z = 334.900665;
    //currCamera->pos.x = 0.f;

    renderer->renderObject(floorObj);
    //renderer->renderObject(headObj);
}

void UpdateElapsedTime()
{
    static int lastTime = clock();

    int currentTimestamp = clock();

    elapsedTime = float(currentTimestamp - lastTime);

    lastTime = currentTimestamp;
}
