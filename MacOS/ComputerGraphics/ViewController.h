//
//  ViewController.h
//  ComputerGraphics
//
//  Created by Aleksandr Borodulin on 25/03/2019.
//  Copyright © 2019 Aleksandr Borodulin. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "DisplayImageView.h"

@interface ViewController : NSViewController<DisplayImageViewDelegate>
{
    CVDisplayLinkRef displayLink;
}

@property (weak) IBOutlet DisplayImageView *imageView;

@property (retain, nonatomic) NSString *filePath;

@property (retain, nonatomic) NSBitmapImageRep *imageRep;

@property (retain, nonatomic) NSImage* displayImage;
@property (weak) IBOutlet NSTextField *fpsLabel;
    
-(void)update;
-(void)callback;

@end

