//
//  AppDelegate.h
//  ComputerGraphics
//
//  Created by Aleksandr Borodulin on 25/03/2019.
//  Copyright © 2019 Aleksandr Borodulin. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate>


@end

