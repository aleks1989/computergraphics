//
//  AppDelegate.m
//  ComputerGraphics
//
//  Created by Aleksandr Borodulin on 25/03/2019.
//  Copyright © 2019 Aleksandr Borodulin. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
}


- (void)applicationWillTerminate:(NSNotification *)aNotification {
    // Insert code here to tear down your application
}


@end
