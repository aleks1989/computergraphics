//
//  DisplayView.h
//  ComputerGraphics
//
//  Created by Aleksandr Borodulin on 01/04/2019.
//  Copyright © 2019 Aleksandr Borodulin. All rights reserved.
//

#import <Cocoa/Cocoa.h>

NS_ASSUME_NONNULL_BEGIN

@interface DisplayView : NSView

@end

NS_ASSUME_NONNULL_END
