//
//  DisplayImageView.m
//  ComputerGraphics
//
//  Created by Aleksandr Borodulin on 26/04/2019.
//  Copyright © 2019 Aleksandr Borodulin. All rights reserved.
//

#import "DisplayImageView.h"

@implementation DisplayImageView

- (BOOL)isFlipped {
    return YES;
}

- (void)drawRect:(NSRect)dirtyRect {
    [super drawRect:dirtyRect];
    
    // Drawing code here.
}

- (void)setFrameSize:(NSSize)newSize {
    [super setFrameSize:newSize];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(displayImageSizeChanged)]) {
        [self.delegate displayImageSizeChanged];
    }
}

@end
