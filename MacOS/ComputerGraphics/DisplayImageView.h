//
//  DisplayImageView.h
//  ComputerGraphics
//
//  Created by Aleksandr Borodulin on 26/04/2019.
//  Copyright © 2019 Aleksandr Borodulin. All rights reserved.
//

#import <Cocoa/Cocoa.h>

NS_ASSUME_NONNULL_BEGIN

@protocol DisplayImageViewDelegate<NSObject>
@optional
- (void)displayImageSizeChanged;
@end

@interface DisplayImageView : NSImageView

- (BOOL)isFlipped;

@property (nonatomic, weak) id<DisplayImageViewDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
