//
//  main.m
//  ComputerGraphics
//
//  Created by Aleksandr Borodulin on 25/03/2019.
//  Copyright © 2019 Aleksandr Borodulin. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
    return NSApplicationMain(argc, argv);
}
