//
//  ViewController.m
//  ComputerGraphics
//
//  Created by Aleksandr Borodulin on 25/03/2019.
//  Copyright © 2019 Aleksandr Borodulin. All rights reserved.
//

#define MAC_OS
//#define USE_OPEN_CL

#import "ViewController.h"
#include "tgaimage.h"
#include "model.h"
#include "Render.h"
#include <iostream>
#include <cmath>
#include <stdio.h>
#include <math.h>
#include "Camera.hpp"

const RGBA white = RGBA(255, 255, 255, 255);
const RGBA red = RGBA(255, 0, 0, 255);
const RGBA green = RGBA(0, 255, 0, 255);
const RGBA blue = RGBA(0, 0, 255, 255);

Vec3f camera(0,0,0);

BOOL wKeyDown = false;
BOOL sKeyDown = false;
BOOL aKeyDown = false;
BOOL dKeyDown = false;

BSPTree* headBSP;
BSPTree* floorBSP;
Model<double>* floorObj;
Model<double>* headObj;
Model<float>* headObjf;
Model<float>* floorObjf;

float floorZRotate = 0;

Render* renderer;
Camera* currCamera;

BOOL showBSP = true;

void renderObjects()
{
    if (currCamera == nullptr) {
        return;
    }
    
    if (!renderer)
    {
        renderer = new Render(currCamera);
    }
    
    //currCamera->phi -= 0.005;
    
    renderer->cleanRenderBuffer();
    renderer->cleanZBuffer();
    
    //floorZRotate -= 0.001;
    
    //currCamera->pos.z = 233.795792;
    //currCamera->pos.x = -133.210678;
    //currCamera->phi = 1.53999794;
    
    if (showBSP) {
        floorBSP->objMat = Mat4_4::rotateX(floorZRotate) * Mat4_4::translate(0.f, -100.f, 250)/* * Mat4_4::rotateX(1.5708)*/;
        renderer->renderBSP(floorBSP);
        
        headBSP->objMat =  Mat4_4::rotateX(floorZRotate) * Mat4_4::translate(0.f, 0.f, 200)/* * Mat4_4::scale(100.f, 100.f, 100.f)*/;
        renderer->renderBSP(headBSP);
    } else {
        floorObjf->objMat = Mat4_4::rotateX(floorZRotate) * Mat4_4::translate(0.f, -100.f, 250)/* * Mat4_4::rotateX(1.5708)*/;
        renderer->renderObject(floorObjf);
        
        headObjf->objMat =  Mat4_4::rotateX(floorZRotate) * Mat4_4::translate(0.f, 0.f, 200)/* * Mat4_4::scale(100.f, 100.f, 100.f)*/;
        renderer->renderObject(headObjf);
    }
}

@implementation ViewController

static NSInteger deltaTime;
static NSInteger lastTime;
    
static int frames = 0;
static double starttime = 0;
static bool first = true;
static float fps = 0.0f;

NSObject *resizeNotificationObject;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.imageView.delegate = self;
    
    NSString * headRoot = [[NSBundle mainBundle] pathForResource:@"african_head" ofType:@"obj"];
    char const *head_path_cstr = [[NSFileManager defaultManager] fileSystemRepresentationWithPath:headRoot];
    
    if (showBSP) {
        headObj = new Model<double>(head_path_cstr, Vec3df(100.0, 100.0, 100.0), [self createTextureForName:@"african_head_diffuse"]);
        headBSP = new BSPTree(headObj);
    } else {
        headObjf = new Model<float>(head_path_cstr, Vec3f(100.0, 100.0, 100.0), [self createTextureForName:@"african_head_diffuse"]);
    }
    
    Vec4f p0 = Mat3_3::rotateX(0.2) * Vec3f(-100.f, 16.f, 0.f);
    Vec4f p1 = Mat3_3::rotateX(0.2) * Vec3f(100.f, 16.f, 0.f);
    Vec4f p2 = Mat3_3::rotateX(0.2) * Vec3f(100.f, 0.f, -200.f);
    Vec4f p3 = Mat3_3::rotateX(0.2) * Vec3f(-100.f, 16.f, -200.f);
    
    if (showBSP) {
        std::vector<Vec3df> vertices = {
            Vec3df(-100.f, 200.f, 0.f), Vec3df(100.f, 200.f, 0.f), Vec3df(100.f, 0.f, 0.f), Vec3df(-100.f, 0.f, 0.f),
            Vec3df(p0.x, p0.y, p0.z), Vec3df(p1.x, p1.y, p1.z), Vec3df(p2.x, p2.y, p2.z), Vec3df(p3.x, p3.y, p3.z)
        };
        
        std::vector<Vec2f> texCoords = {
            Vec2f(0.f, 0.f), Vec2f(1.f, 0.f), Vec2f(1.f, 1.f), Vec2f(0.f, 1.f),
            Vec2f(0.f, 0.f), Vec2f(1.f, 0.f), Vec2f(1.f, 1.f), Vec2f(0.f, 1.f)
        };
        
        Face face1(FVertex(2, 2), FVertex(1, 1), FVertex(0, 0));
        Face face2(FVertex(2, 2), FVertex(0, 0), FVertex(3, 3));
        Face face3(FVertex(2 + 4, 2 + 4), FVertex(1 + 4, 1 + 4), FVertex(0 + 4, 0 + 4));
        Face face4(FVertex(2 + 4, 2 + 4), FVertex(0 + 4, 0 + 4), FVertex(3 + 4, 3 + 4));
        
        std::vector<Face> faces;
        
        //faces.push_back(face1);
        faces.push_back(face2);
        faces.push_back(face3);
        faces.push_back(face4);
        
        
        floorObj = new Model<double>(vertices, texCoords, faces, [self createTextureForName:@"floor_diffuse"]);
        //-1.5708
        floorBSP = new BSPTree(floorObj);
    } else {
        std::vector<Vec3f> vertices = {
            Vec3f(-100.f, 200.f, 0.f), Vec3f(100.f, 200.f, 0.f), Vec3f(100.f, 0.f, 0.f), Vec3f(-100.f, 0.f, 0.f),
            Vec3f(p0.x, p0.y, p0.z), Vec3f(p1.x, p1.y, p1.z), Vec3f(p2.x, p2.y, p2.z), Vec3f(p3.x, p3.y, p3.z)
        };
        
        std::vector<Vec2f> texCoords = {
            Vec2f(0.f, 0.f), Vec2f(1.f, 0.f), Vec2f(1.f, 1.f), Vec2f(0.f, 1.f),
            Vec2f(0.f, 0.f), Vec2f(1.f, 0.f), Vec2f(1.f, 1.f), Vec2f(0.f, 1.f)
        };
        
        Face face1(FVertex(2, 2), FVertex(1, 1), FVertex(0, 0));
        Face face2(FVertex(2, 2), FVertex(0, 0), FVertex(3, 3));
        Face face3(FVertex(2 + 4, 2 + 4), FVertex(1 + 4, 1 + 4), FVertex(0 + 4, 0 + 4));
        Face face4(FVertex(2 + 4, 2 + 4), FVertex(0 + 4, 0 + 4), FVertex(3 + 4, 3 + 4));
        
        std::vector<Face> faces;
        
        faces.push_back(face1);
        faces.push_back(face2);
        faces.push_back(face3);
        faces.push_back(face4);
        
        
        floorObjf = new Model<float>(vertices, texCoords, faces, [self createTextureForName:@"floor_diffuse"]);
    }
    
    
    currCamera = new Camera(1, 500, self.imageView.frame.size.width, self.imageView.frame.size.height);
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(windowDidResize:) name:NSWindowDidResizeNotification object:nil];
    
    [NSTimer scheduledTimerWithTimeInterval:1.f/60.f
                                     target:self
                                   selector:@selector(callback)
                                   userInfo:nil
                                    repeats:YES];
}

- (void)viewDidAppear {
    [super viewDidAppear];
    
    [self.view.window makeFirstResponder:self];
    
    currCamera->setScreenSize(self.imageView.frame.size.width, self.imageView.frame.size.height);
    renderObjects();
    
    self.imageRep =
    [[NSBitmapImageRep alloc]
     initWithBitmapDataPlanes: nil  // allocate the pixel buffer for us
     pixelsWide: currCamera->screenWidth()
     pixelsHigh: currCamera->screenHeight()
     bitsPerSample: 8
     samplesPerPixel: 4
     hasAlpha: YES
     isPlanar: NO
     colorSpaceName: NSCalibratedRGBColorSpace
     bytesPerRow: 4 * currCamera->screenWidth()     // passing 0 means "you figure it out"
     bitsPerPixel: 32];   // this must agree with bitsPerSample and samplesPerPixel
    
    CVDisplayLinkStart(displayLink);
}

- (texture_shared_ptr)createTextureForName:(NSString*) name {
    NSString *headTextureRoot = [[NSBundle mainBundle] pathForResource:name ofType:@"tga"];
    const char *texture_path_cstr = [[NSFileManager defaultManager] fileSystemRepresentationWithPath:headTextureRoot];
    
    return Texture::CreateTexture(texture_path_cstr);
}

- (void)dealloc
{
    delete renderer;
    delete headBSP;
    delete floorObj;
    delete headObj;
    delete currCamera;
    delete headObjf;
    delete floorObjf;
    CVDisplayLinkRelease(displayLink);
}


- (void)setRepresentedObject:(id)representedObject {
    [super setRepresentedObject:representedObject];
}

- (void)windowDidResize:(NSNotification *)notification {
}

- (BOOL)acceptsFirstResponder {
    return YES;
}

- (BOOL)becomeFirstResponder {
    return YES;
}

- (void)keyDown:(NSEvent *)event {
    if ([event.characters isEqualToString:@"w"]) {
        wKeyDown = true;
    } else if ([event.characters isEqualToString:@"s"]) {
        sKeyDown = true;
    } else if ([event.characters isEqualToString:@"a"]) {
        aKeyDown = true;
    } else if ([event.characters isEqualToString:@"d"]) {
        dKeyDown = true;
    }
}

- (void)keyUp:(NSEvent *)event {
    if ([event.characters isEqualToString:@"w"]) {
        wKeyDown = false;
    } else if ([event.characters isEqualToString:@"s"]) {
        sKeyDown = false;
    } else if ([event.characters isEqualToString:@"a"]) {
        aKeyDown = false;
    } else if ([event.characters isEqualToString:@"d"]) {
        dKeyDown = false;
    }
}

- (void)mouseDragged:(NSEvent *)event {
    currCamera->phi -= [event deltaX] * 0.002;
}

-(void)update {
    if (wKeyDown) {
        currCamera->pos.z += deltaTime * 0.05 * cos(currCamera->phi);
        
        currCamera->pos.x += deltaTime * 0.05 * sin(currCamera->phi);
    }
    if (sKeyDown) {
        currCamera->pos.z -= deltaTime * 0.05 * cos(currCamera->phi);
        
        currCamera->pos.x -= deltaTime * 0.05 * sin(currCamera->phi);
    }
    if (aKeyDown) {
        currCamera->pos.z += deltaTime * 0.05 * sin(currCamera->phi);
        
        currCamera->pos.x -= deltaTime * 0.05 * cos(currCamera->phi);
    }
    if (dKeyDown) {
        currCamera->pos.z -= deltaTime * 0.05 * sin(currCamera->phi);
        
        currCamera->pos.x += deltaTime * 0.05 * cos(currCamera->phi);
    }
    
    renderObjects();
    
    int idx = 283 + 168 * currCamera->screenWidth();
    renderer->renderBuffer[idx] = RGBA(0xFF0000FF);
    
    int* pix = (int*)[self.imageRep bitmapData];
    
    if (pix) {
        for ( int i = 0; i < currCamera->screenHeight(); ++i )
        {
            for ( int j = 0; j < currCamera->screenWidth(); ++j )
            {
                int idx = i * currCamera->screenWidth() + j;
                int invIdx = (currCamera->screenHeight() - i - 1) * currCamera->screenWidth() + j;
                RGBA color = renderer->renderBuffer[idx];
                pix[invIdx] = color.value;
            }
        }
    }
    
    self.displayImage = [[NSImage alloc] initWithSize:NSMakeSize(currCamera->screenWidth_f(),currCamera->screenHeight_f())];
    [self.displayImage addRepresentation:self.imageRep];
    
    self.imageView.image = self.displayImage;
}

-(void)callback
{
    NSInteger timestamp = [[NSDate date] timeIntervalSince1970] * 1000;
    deltaTime = timestamp - lastTime;
    lastTime = timestamp;
    
    ViewController* vc = self;
    
    [vc update];
    
    if (first)
    {
        frames = 0;
        starttime = timestamp;
        first = FALSE;
        return;
    }
    
    frames++;
    if (timestamp - starttime > 1000 && frames > 10)
    {
        fps = (double) frames / ((timestamp - starttime) * 0.001);
        [vc.fpsLabel setStringValue: [NSString stringWithFormat:@"FPS: %.2f", fps]];
        starttime = timestamp;
        frames = 0;
    }
}

#pragma mark - Delegate

- (void)displayImageSizeChanged {
    if (currCamera && renderer) {
        currCamera->setScreenSize(self.imageView.frame.size.width, self.imageView.frame.size.height);
        self.imageRep =
        [[NSBitmapImageRep alloc]
         initWithBitmapDataPlanes: nil  // allocate the pixel buffer for us
         pixelsWide: currCamera->screenWidth()
         pixelsHigh: currCamera->screenHeight()
         bitsPerSample: 8
         samplesPerPixel: 4
         hasAlpha: YES
         isPlanar: NO
         colorSpaceName: NSCalibratedRGBColorSpace
         bytesPerRow: 4 * currCamera->screenWidth()     // passing 0 means "you figure it out"
         bitsPerPixel: 32];   // this must agree with bitsPerSample and samplesPerPixel
        renderer->resetBuffers();
        [self update];
    }
}

#pragma mark - Action

- (IBAction) projDistDidChange: (id) sender
{
    currCamera->pos.z += deltaTime * 0.1;
}

@end
